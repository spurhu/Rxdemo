package view;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import util.TESTDemo;
import util.TimeUtil;

import static org.junit.Assert.assertEquals;

/**
 * @author: huyahui
 * @date: 2018/3/14
 */

public class TimeUtilTest {


    @BeforeClass
    public static void beforeClass(){
        System.out.println("beforeClass");
    }

    @Before
    public void before(){
        System.out.println("before");
    }



    /**
     * 运行的测试方法，注意需要使用@Test注解
     */
    @Test
    public void timeFormat_isCorrect() throws Exception {
        long time = 1505720213000L;
        //预期的结果
        String result = "2017/09/18 15:36:53";
        System.out.println("test----结果:"+result);
        assertEquals(result, TESTDemo.formateTime(time));
    }

    @Test
    public void testWo(){
        System.out.println("test----testWo");
    }


    @After
    public void after(){
        System.out.println("after");
    }

    @AfterClass
    public static void afterClass(){
        System.out.println("afterClass");
    }




}
