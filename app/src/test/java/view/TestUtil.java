package view;

import org.junit.Ignore;
import org.junit.Test;

import test.MessageUtil;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;

/**
 * @author: huyahui
 * @date: 2018/3/15
 */

public class TestUtil {


    String msg="Hello World";

    MessageUtil messageUtil=new MessageUtil(msg);

    int[] list = {36, 28, 45, 13, 67, 37, 18, 56};
    int[] list2 = {13, 18, 28, 36, 37, 45, 56, 67};
    int[] list3 = {15, 18, 28, 36, 37, 45, 56, 67};


    @Test
    public void testPrintMsg(){
       assertEquals(msg,messageUtil.printMsg());
    }

    @Ignore
    @Test
    public void testPrintMsg2(){
        assertEquals("---------",messageUtil.printMsg());
    }

    @Test
    public void testSort(){
        assertArrayEquals(list2,messageUtil.bubbleSort(list));
    }



}
