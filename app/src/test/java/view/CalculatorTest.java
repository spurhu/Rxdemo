package view;

import android.test.suitebuilder.annotation.MediumTest;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * 单元测试类
 * @author: huyahui
 * @date: 2018/3/14
 */
@MediumTest
public class CalculatorTest {


    private Calculator mCalculator;

    /**
     * new出你要测试的目标类
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        mCalculator = new Calculator();
    }


    @Test
    public void sum() throws Exception {
        /**
         * 6d 逾期结果    方法调用
         */
        assertEquals(6d, mCalculator.sum(1d, 5d), 0);
    }

    /**
     * 减法
     * @throws Exception
     */
    @Test
    public void substract() throws Exception {
        assertEquals(1d, mCalculator.substract(5d, 4d), 0);
    }

    /**
     * 除法
     * @throws Exception
     */
    @Test
    public void divide() throws Exception {
        assertEquals(3d, mCalculator.divide(20d, 5d), 1);
    }

    @Test
    public void multiply() throws Exception {
        assertEquals(10d, mCalculator.multiply(2d, 5d), 1);
    }


    /**
     * 断言的使用
     */
    @Test
    public void hu(){
//        assertEquals(3,3);
//        assertTrue("条件不成立",2==2);
        int[] i={3,5,8,6};
        int[] j={3,5,8,6};
        assertArrayEquals(i,j);
    }



}