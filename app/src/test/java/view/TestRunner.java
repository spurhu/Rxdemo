package view;

import com.sonnyjack.utils.toast.ToastUtils;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 * @author: huyahui
 * @date: 2018/3/15
 */

public class TestRunner {

    public static void main(String[] args) {
          //测试一个类
//        Result result = JUnitCore.runClasses(TestUtil.class);
        //套件测试
        Result result = JUnitCore.runClasses(JunitTestSuite.class);
        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }
        System.out.println(result.wasSuccessful());
    }

}
