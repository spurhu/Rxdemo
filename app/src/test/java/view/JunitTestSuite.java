package view;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author: huyahui
 * @date: 2018/3/15
 */



@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestUtil.class,
        TestUtil2.class
})
public class JunitTestSuite {

}
