package video.bean;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.example.huyahui.rxdemo.R;
import com.google.android.exoplayer2.C;

import butterknife.BindView;
import butterknife.ButterKnife;
import entity.VideoModel;
import util.SmallVideoHelper;

/**
 * @author: huyahui
 * @date: 2018/2/6
 */

public class RecyclerItemViewHolder extends             RecyclerItemBaseHolder {

    private final static String TAG="RecyclerItemViewHolder";
    private Context context=null;

    @BindView(R.id.frame_container)
    FrameLayout listItemContainer;
    @BindView(R.id.recycler_item)
    ImageView listItemBtn;

    ImageView imageView;
    private SmallVideoHelper smallVideoHelper;
    private  SmallVideoHelper.GSYSmallVideoHelperBuilder gsySmallVideoHelperBuilder;

    public RecyclerItemViewHolder(Context context,View view) {
        super(view);
        this.context=context;
        ButterKnife.bind(this,view);
        imageView=new ImageView(context);
    }


    public void onBind(final int position, VideoModel videoModel){
        //增加封面
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageResource(R.mipmap.yuanyuan);

        smallVideoHelper.addVideoPlayer(position,imageView,TAG,listItemContainer,listItemBtn);
        listItemBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRecyclerBaseAdapter().notifyDataSetChanged();
                smallVideoHelper.setPlayPositionAndTag(position,TAG);
                String url;
                //根据位置是不是2的整数倍来放置视频地址
                if (position % 2 == 0) {
//                    url = "https://res.exexm.com/cw_145225549855002";
                    url="http://data.vod.itc.cn/?rb=1&key=jbZhEJhlqlUN-Wj_HEI8BjaVqKNFvDrn&prod=flash&pt=1&new=/190/249/qCvmtO48otxI5Y8T1RNvqF.mp4";
                } else {
                    url = "http://7xse1z.com1.z0.glb.clouddn.com/1491813192";
                }
                gsySmallVideoHelperBuilder.setVideoTitle("title"+position).setUrl(url);
                smallVideoHelper.startPlay();
            }
        });


    }

    @Override
    public RecyclerView.Adapter getRecyclerBaseAdapter() {
        return super.getRecyclerBaseAdapter();
    }


    @Override
    public void setRecyclerBaseAdapter(RecyclerView.Adapter recyclerBaseAdapter) {
        super.setRecyclerBaseAdapter(recyclerBaseAdapter);
    }


    public void setVideoHelper(SmallVideoHelper smallVideoHelper,SmallVideoHelper.GSYSmallVideoHelperBuilder gsySmallVideoHelperBuilder){
        this.smallVideoHelper=smallVideoHelper;
        this.gsySmallVideoHelperBuilder=gsySmallVideoHelperBuilder;
    }



}
