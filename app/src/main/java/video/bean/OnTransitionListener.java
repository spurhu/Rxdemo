package video.bean;

import android.annotation.TargetApi;
import android.os.Build;
import android.transition.Transition;

/**
 * @author: huyahui
 * @date: 2018/2/5
 */
@TargetApi(Build.VERSION_CODES.KITKAT)
public class OnTransitionListener implements Transition.TransitionListener {


    @Override
    public void onTransitionStart(Transition transition) {

    }

    @Override
    public void onTransitionEnd(Transition transition) {

    }

    @Override
    public void onTransitionCancel(Transition transition) {

    }

    @Override
    public void onTransitionPause(Transition transition) {

    }

    @Override
    public void onTransitionResume(Transition transition) {

    }
}