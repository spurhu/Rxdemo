package video.bean;

/**
 * @author: huyahui
 * @date: 2018/2/5
 */

public class SwitchVideoModel {

    private String url;
    private String name;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SwitchVideoModel(String url, String name) {
        this.url = url;
        this.name = name;
    }

    @Override
    public String toString() {
        return "SwitchVideoModel{" +
                "url='" + url + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
