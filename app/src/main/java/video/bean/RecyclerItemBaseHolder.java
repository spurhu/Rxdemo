package video.bean;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * @author: huyahui
 * @date: 2018/2/6
 */

public class RecyclerItemBaseHolder extends RecyclerView.ViewHolder {

    RecyclerView.Adapter adapter;

    public RecyclerItemBaseHolder(View view){
        super(view);
    }


    public RecyclerView.Adapter getRecyclerBaseAdapter(){
        return adapter;
    }


    public void setRecyclerBaseAdapter(RecyclerView.Adapter recyclerBaseAdapter){
    this.adapter=recyclerBaseAdapter;
    }

}
