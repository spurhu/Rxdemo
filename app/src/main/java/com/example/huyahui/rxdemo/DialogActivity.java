package com.example.huyahui.rxdemo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.InputType;
import android.text.Spanned;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.liji.circleimageview.CircleImageView;
import com.qmuiteam.qmui.widget.dialog.QMUIBottomSheet;
import com.qmuiteam.qmui.widget.dialog.QMUIDialog;
import com.qmuiteam.qmui.widget.dialog.QMUIDialogAction;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.Provides;
import tyrantgit.widget.HeartLayout;
import util.MyToast;
import view.DialogManager;
import view.MyVideoView;

import static com.blankj.utilcode.util.ActivityUtils.startActivity;

public class DialogActivity extends AppCompatActivity {

    @BindView(R.id.btn_dailog_nornmal)
    Button btnDailogNornmal;
    @BindView(R.id.btn_dailog_menu)
    Button btnDailogMenu;
    @BindView(R.id.btn_dailog_checkboc)
    Button btnDailogCheckboc;
    @BindView(R.id.btn_dailog_morechoose)
    Button btnDailogMorechoose;
    @BindView(R.id.btn_dailog4)
    Button btnDailog4;
    @BindView(R.id.btn_list)
    Button btnList;
    @BindView(R.id.btn_grid)
    Button btnGrid;
    @BindView(R.id.tv_rxbus)
    Button tvRxbus;
    @BindView(R.id.tv_customdialog)
    Button tvCustomdialog;
    @BindView(R.id.img)
    CircleImageView circleImageView;
    @BindView(R.id.tvHtml)
    TextView tvHtml;
    @BindView(R.id.heart_layout )
    HeartLayout mHeartLayout ;

    private Random mRandom = new Random();
    private Timer mTimer = new Timer();

    String htmls="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;不错的车111<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;< img src=\"c=\"https://images.unsplash.com/photo-1494905998402-395d579af36f?ixlib=rb-0.3.5&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;s=40085e3ea5d8524b7518df890e4aa72d&amp;auto=format&amp;fit=crop&amp;w=2850&amp;q=80\" st\" style=\"color:rgb(68, 68, 68); font-size:12px; text-align:right; max-width:30%;\"></p ><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 我要买";

    StringBuffer sb=new StringBuffer("");
    String strHtml=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
        ButterKnife.bind(this);

        Glide.with(this).load("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1535711035026&di=b543fc42046500339107667bad46fdc5&imgtype=0&src=http%3A%2F%2Fe.hiphotos.baidu.com%2Fzhidao%2Fwh%253D680%252C800%2Fsign%3Ded1939b46c09c93d07a706f1a70dd4e4%2F30adcbef76094b3688a16857a8cc7cd98d109d5c.jpg").into(circleImageView);

        NetworkImageGetter networkImageGetter=new NetworkImageGetter();
        Spanned spanned = Html.fromHtml(htmls, networkImageGetter, null);
        // 图片文字居中显示
        tvHtml.setGravity(Gravity.CENTER_HORIZONTAL);
        //tvHtml.setText(Html.fromHtml(htmls));
        tvHtml.setText(spanned);


        initLikeAnimation();


    }

    private void initLikeAnimation() {
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                mHeartLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        mHeartLayout.addHeart(randomColor());
                    }
                });
            }
            }, 50, 50);
    }

    private final class NetworkImageGetter implements Html.ImageGetter {

        @Override
        public Drawable getDrawable(String source) {
            // TODO Auto-generated method stub

            LevelListDrawable d = new LevelListDrawable();
            new LoadImage().execute(source, d);
            return d;
        }

    }


    /**** 异步加载图片 **/
    private final class LoadImage extends AsyncTask<Object, Void, Bitmap> {

        private LevelListDrawable mDrawable;

        @Override
        protected Bitmap doInBackground(Object... params) {
            String source = (String) params[0];
            mDrawable = (LevelListDrawable) params[1];

            try {
                InputStream is = new URL(source).openStream();
                return BitmapFactory.decodeStream(is);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {

            if (bitmap != null) {
                BitmapDrawable d = new BitmapDrawable(bitmap);
                mDrawable.addLevel(1, 1, d);
                mDrawable
                        .setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
                // mDrawable.setBounds(0, 0,
                // getWindowManager().getDefaultDisplay().getWidth(),
                // bitmap.getHeight());
                mDrawable.setLevel(1);
                CharSequence t = tvHtml.getText();
                tvHtml.setText(t);
            }
        }
    }




    /**
     * 开始跳转
     *
     * @param context
     */
    public static void start(Context context) {
        Intent intent = new Intent(context, DialogActivity.class);
        context.startActivity(intent);
    }


    @OnClick({R.id.btn_dailog_nornmal, R.id.btn_dailog_menu, R.id.btn_dailog_checkboc, R.id.btn_dailog_morechoose,
            R.id.btn_dailog4, R.id.btn_list, R.id.btn_grid,R.id.tv_rxbus,R.id.tv_customdialog})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_dailog_nornmal:
                //showMessagePositiveDialog();
                initLikeAnimation();
                break;
            case R.id.btn_dailog_menu:
                if (mTimer != null) {
                    mTimer.cancel();
                }

                //showMenuDialog();
                break;
            case R.id.btn_dailog_checkboc:
                showConfirmMessageDialog();
                break;
            case R.id.btn_dailog_morechoose:
                showMultiChoiceDialog();
                break;
            case R.id.btn_dailog4:
                showEditTextDialog();
                break;
            case R.id.btn_list:
                showSimpleBottomSheetList();
                break;
            case R.id.btn_grid:
                showSimpleBottomSheetGrid();
                break;
            case R.id.tv_rxbus:
                MyToast.showMessage(this, "-----jnjkbkj--------");
                break;
            case R.id.tv_customdialog:
                DialogManager.showConfirmDialog(DialogActivity.this, "这是对话框", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MyToast.showMessage(DialogActivity.this,"弹出对话框");
                    }
                });
                break;
            default:
                break;
        }
    }


    /**
     * 普通对话框
     */
    private void showMessagePositiveDialog() {
        new QMUIDialog.MessageDialogBuilder(this)
                .setTitle("标题")
                .setMessage("确定要发送吗？")
                .addAction("取消", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        dialog.dismiss();
                    }
                })
                .addAction("确定", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        dialog.dismiss();
                        Toast.makeText(DialogActivity.this, "发送成功", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }


    /**
     * 菜单对话框,选项太多可以滚动
     */
    private void showMenuDialog() {
        final String[] items = new String[]{"选项1", "选项2", "选项3"};
        QMUIDialog qmuiDialog=new QMUIDialog.MenuDialogBuilder(this)
                .addItem("              完成", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })
                .addItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(DialogActivity.this, "你选择了 " + items[which], Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                })
                .show();
    }


    /**
     * 带checkbox的对话框
     */
    private void showConfirmMessageDialog() {
        new QMUIDialog.CheckBoxMessageDialogBuilder(this)
                .setTitle("退出后是否删除账号信息?")
                .setMessage("删除账号信息")
                .setChecked(true)
                .addAction("取消", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        dialog.dismiss();
                    }
                })
                .addAction("退出", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        dialog.dismiss();
                    }
                })
                .show();
    }


    /**
     * 多选
     */
    private void showMultiChoiceDialog() {
        final String[] items = new String[]{"选项1", "选项2", "选项3", "选项4", "选项5", "选项6"};
        final QMUIDialog.MultiCheckableDialogBuilder builder = new QMUIDialog.MultiCheckableDialogBuilder(this)
                .setCheckedItems(new int[]{1, 3})
                .addItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        builder.addAction("取消", new QMUIDialogAction.ActionListener() {
            @Override
            public void onClick(QMUIDialog dialog, int index) {
                dialog.dismiss();
            }
        });
        builder.addAction("提交", new QMUIDialogAction.ActionListener() {
            @Override
            public void onClick(QMUIDialog dialog, int index) {
                String result = "你选择了 ";
                for (int i = 0; i < builder.getCheckedItemIndexes().length; i++) {
                    result += "" + builder.getCheckedItemIndexes()[i] + "; ";
                }
                Toast.makeText(DialogActivity.this, result, Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        builder.show();
    }


    /**
     * 带输入框的对话框
     */
    private void showEditTextDialog() {
        final QMUIDialog.EditTextDialogBuilder builder = new QMUIDialog.EditTextDialogBuilder(DialogActivity.this);
        builder.setTitle("标题")
                .setPlaceholder("在此输入您的昵称")
                .setInputType(InputType.TYPE_CLASS_TEXT)
                .addAction("取消", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        dialog.dismiss();
                    }
                })
                .addAction("确定", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        CharSequence text = builder.getEditText().getText();
                        if (text != null && text.length() > 0) {
                            Toast.makeText(DialogActivity.this, "您的昵称: " + text, Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        } else {
                            Toast.makeText(DialogActivity.this, "请填入昵称", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .show();
    }


    // ================================ 生成不同类型的BottomSheet
    private void showSimpleBottomSheetList() {
        new QMUIBottomSheet.BottomListSheetBuilder(this)
                .addItem("Item 1")
                .addItem("Item 2")
                .addItem("Item 3")
                .setOnSheetItemClickListener(new QMUIBottomSheet.BottomListSheetBuilder.OnSheetItemClickListener() {
                    @Override
                    public void onClick(QMUIBottomSheet dialog, View itemView, int position, String tag) {
                        dialog.dismiss();
                        Toast.makeText(DialogActivity.this, "Item " + (position + 1), Toast.LENGTH_SHORT).show();
                    }
                })
                .build()
                .show();
    }

    private void showSimpleBottomSheetGrid() {
        final int TAG_SHARE_WECHAT_FRIEND = 0;
        final int TAG_SHARE_WECHAT_MOMENT = 1;
        final int TAG_SHARE_WEIBO = 2;
        final int TAG_SHARE_CHAT = 3;
        final int TAG_SHARE_LOCAL = 4;
        QMUIBottomSheet.BottomGridSheetBuilder builder = new QMUIBottomSheet.BottomGridSheetBuilder(this);
        builder.addItem(R.mipmap.icon_more_operation_share_friend, "分享到微信", TAG_SHARE_WECHAT_FRIEND, QMUIBottomSheet.BottomGridSheetBuilder.FIRST_LINE)
                .addItem(R.mipmap.icon_more_operation_share_moment, "分享到朋友圈", TAG_SHARE_WECHAT_MOMENT, QMUIBottomSheet.BottomGridSheetBuilder.FIRST_LINE)
                .addItem(R.mipmap.icon_more_operation_share_weibo, "分享到微博", TAG_SHARE_WEIBO, QMUIBottomSheet.BottomGridSheetBuilder.FIRST_LINE)
                .addItem(R.mipmap.icon_more_operation_share_chat, "分享到私信", TAG_SHARE_CHAT, QMUIBottomSheet.BottomGridSheetBuilder.FIRST_LINE)
                .addItem(R.mipmap.icon_more_operation_save, "保存到本地", TAG_SHARE_LOCAL, QMUIBottomSheet.BottomGridSheetBuilder.SECOND_LINE)
                .setOnSheetItemClickListener(new QMUIBottomSheet.BottomGridSheetBuilder.OnSheetItemClickListener() {
                    @Override
                    public void onClick(QMUIBottomSheet dialog, View itemView) {
                        dialog.dismiss();
                        int tag = (int) itemView.getTag();
                        switch (tag) {
                            case TAG_SHARE_WECHAT_FRIEND:
                                Toast.makeText(DialogActivity.this, "分享到微信", Toast.LENGTH_SHORT).show();
                                break;
                            case TAG_SHARE_WECHAT_MOMENT:
                                Toast.makeText(DialogActivity.this, "分享到朋友圈", Toast.LENGTH_SHORT).show();
                                break;
                            case TAG_SHARE_WEIBO:
                                Toast.makeText(DialogActivity.this, "分享到微博", Toast.LENGTH_SHORT).show();
                                break;
                            case TAG_SHARE_CHAT:
                                Toast.makeText(DialogActivity.this, "分享到私信", Toast.LENGTH_SHORT).show();
                                break;
                            case TAG_SHARE_LOCAL:
                                Toast.makeText(DialogActivity.this, "保存到本地", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }).build().show();


    }



    public void goMarket() {
        //这里开始执行一个应用市场跳转逻辑，默认this为Context上下文对象
        Intent intent = new Intent(Intent.ACTION_VIEW);
        //跳转到应用市场，非Google Play市场一般情况也实现了这个接口
        intent.setData(Uri.parse("market://details?id=" + "com.fcity"));
        //存在手机里没安装应用市场的情况，跳转会包异常，做一个接收判断  可以接收
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else { //没有应用市场，我们通过浏览器跳转到Google Play
            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + "com.fcity"));
            //这里存在一个极端情况就是有些用户浏览器也没有，再判断一次   有浏览器
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            } else { //天哪，这还是智能手机吗？
                Toast.makeText(this, "天啊，您没安装应用市场，连浏览器也没有，您买个手机干啥？", Toast.LENGTH_SHORT).show();
            }
        }
    }




    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    private int randomColor() {
        return Color.rgb(mRandom.nextInt(255), mRandom.nextInt(255), mRandom.nextInt(255));
    }





}
