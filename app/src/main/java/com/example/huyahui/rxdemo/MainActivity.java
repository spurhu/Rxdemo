package com.example.huyahui.rxdemo;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.location.LocationManager;
import android.media.MediaMetadataRetriever;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.gyf.barlibrary.ImmersionBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import entity.RelationSubmitRequestEntity;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;
import util.ContactUtils;
import util.MessageEvent;
import util.RxUtils;
import service.MyService;

/**
 * Demo  class
 *
 * @author: huyahui
 * @date: 2018/1/20
 */
public class MainActivity extends AppCompatActivity {

    @BindView(R.id.tv_text)
    TextView tvText;
    @BindView(R.id.rela)
    RelativeLayout rela;
    @BindView(R.id.btn_qmui)
    Button btnQmui;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.img)
    ImageView img;


    private static final String TAG = "MainActivity";
    /**
     * 通讯录
     */
    String contactsListJson = "";
    private Context context;
    private LocationManager lm;
    String path="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1539413133694&di=57990a994d296ad14c98bce18e502320&imgtype=0&src=http%3A%2F%2Fhbimg.b0.upaiyun.com%2F363d081ff2426eeef84ae4c3fe418a90c0a91eff2ae744-Yl5ZYC_fw658";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        //注册
        EventBus.getDefault().register(this);
        context=getApplicationContext();

//      AnalyticsConfig.setChannel(ChannelUtil.getChannel(this));
//      QMUIStatusBarHelper.setStatusBarLightMode(this);
//      QMUIStatusBarHelper.setStatusBarDarkMode(this);

        rela.setBackgroundResource(R.mipmap.sun);

        initTitlbar();

        //rxjava操作符 RxUtils.
        initView();
        RxUtils.filter();
    }


    private void initView() {
        //判断是否有定位权限
        tvText.setMovementMethod(ScrollingMovementMethod.getInstance());
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent();
                intent.setType("video/*"); //选择视频 （mp4 3gp 是android支持的视频格式）
                // intent.setType("video/*;image/*");//同时选择视频和图片
                /* 使用Intent.ACTION_GET_CONTENT这个Action */
                intent.setAction(Intent.ACTION_GET_CONTENT);
                /* 取得相片后返回本画面 */
                startActivityForResult(intent, 1);
            }
        });

        //:/storage/emulated/0/DCIM/Camera/video.mp4
        // video.setVideoPath("/storage/emulated/0/DCIM/Camera/video.mp4");

        //改变字体颜色
        //setRedText(tvText);

        //解决黑屏
        getWindow().setFormat(PixelFormat.TRANSLUCENT);

        //video.setBackground(getResources().getDrawable(R.drawable.xianghu));
        Glide.with(MainActivity.this).load(path).transition(new DrawableTransitionOptions().crossFade(500)).into(img);

        //获取第一帧缩略图
        //getNetVideoBitmap(img,"https://api.hongrenlian.cn/upload/image/hot/20180925/20180925110414.mp4");
    }


    @OnClick(R.id.btn_qmui)
    public void onViewClicked() {
//        rela.setBackgroundResource(R.mipmap.hall);
       // startActivity(new Intent(MainActivity.this, JsoupDataActivity.class));
        //页面跳转
//         QmuiActivity.start(this);

        Intent intent=new Intent(this,MyService.class);
        startService(intent);

    }


    /**
     * 根据网络视频的url 获取第一帧缩略图
     * @param imageView
     * @param videoUrl
     * @return
     */
    public Bitmap getNetVideoBitmap(ImageView imageView,String videoUrl) {
        Bitmap bitmap = null;
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            //根据url获取缩略图
            retriever.setDataSource(videoUrl, new HashMap());
            bitmap = retriever.getFrameAtTime();
            imageView.setImageBitmap(bitmap);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } finally {
            retriever.release();
        }
        return bitmap;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // 选取图片的返回值
        if (requestCode == 1) {
            //
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();

                getPath(data);

                String[] filePathColumns = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(uri, filePathColumns, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePathColumns[0]);
                String imagePath = c.getString(columnIndex);

                Log.d(TAG, "onActivityResult: "+imagePath);

                /*Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                cursor.moveToFirst();
                // String imgNo = cursor.getString(0); // 图片编号
                String v_path = cursor.getString(1); // 图片文件路径
                String v_size = cursor.getString(2); // 图片大小
                String v_name = cursor.getString(3); // 图片文件名*/


               /* File file = null;
                try {
                    file = new File(new URI(uri.toString()));
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                String filePath = file.getAbsolutePath();
                String[] dataStr = filePath.split("/");
                String fileTruePath = "/sdcard";
                for(int i=4;i<dataStr.length;i++){
                    fileTruePath = fileTruePath+"/"+dataStr[i];
                }*/
//                video.setVideoPath(getRealFilePath(this,uri));
           /*     Log.d(TAG, "onActivityResult: "+v_path+"000"+getRealFilePath(this,uri));
                Log.d(TAG, "onActivityResult: "+v_name);
                Log.d(TAG, "onActivityResult: "+v_size);*/
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }



    public void getPath(Intent data){
        Uri uri = data.getData();//得到uri，后面就是将uri转化成file的过程。
        File file = null;
        file = new File(getRealFilePath(this, uri));
        String filePath = file.getAbsolutePath();
        String[] dataStr = filePath.split("/");
        String fileTruePath = "/sdcard";
        for(int i=4;i<dataStr.length;i++){
            fileTruePath = fileTruePath+"/"+dataStr[i];
        }

        Log.d(TAG, "getPath: "+fileTruePath);
        Toast.makeText(context, ""+fileTruePath, Toast.LENGTH_SHORT).show();


    }



    /**
     * 一个字符串中特定字的颜色
     * 将0.01变为红色
     */
    private void setRedText(TextView tv_tips) {
        SpannableStringBuilder builder = new SpannableStringBuilder("请在您账户中充值0.01元,进行身份验证");
        ForegroundColorSpan span = new ForegroundColorSpan(Color.RED);
        builder.setSpan(span, 8, 12, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv_tips.setText(builder);
    }



    /**
     * 初始化titlebar
     */
    private void initTitlbar() {
        //沉浸式
        ImmersionBar.with(this)
                .titleBar(toolbar, false)
                .transparentBar()
                .init();
    }


    @Override
    protected void onResume() {
        super.onResume();
        //发送事件
        //需求：从ActivityA界面跳转到ActivityB界面后，ActivityB要给ActivityA发送一个消息，ActivityA收到消息后在界面上显示出来
//        EventBus.getDefault().post(new MessageEvent("Hello everyone!"));

        //打印日志
        Timber.tag("main");
        Timber.d("Activity Created");

    }


    String a="0";
    String b="bbb";
    String c="aaaaa";
    String d="66666";
    public void result(){
        if(TextUtils.isEmpty(a)&&TextUtils.isEmpty(b)&&TextUtils.isEmpty(c)&&TextUtils.isEmpty(d)){
            Toast.makeText(context, "有一个是null", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(context, "都不为null", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * 获取网络相关信息
     */
    public void getNetInfo(){
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        //返回所有网络信息
        NetworkInfo[] networkInfos = connectivityManager.getAllNetworkInfo();
        //获取当前激活的网络连接信息
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (Build.VERSION.SDK_INT >= 23) {
            Log.d(TAG, "默认端口：" + connectivityManager.getDefaultProxy() + "getActiveNetwork" + connectivityManager.getActiveNetwork());
        }


        Log.d(TAG, "激活网络信息：" + networkInfo.toString());
        if (networkInfos != null && networkInfos.length > 0) {
            for (int i = 0; i < networkInfos.length; i++) {
                Log.d(TAG, "网络信息：" + networkInfos[i]);
            }
        } else {
            Log.d(TAG, "未取得网络信息：");
            return;
        }
    }


    /**
     * 判断是否定位权限
     */
    public void getPersionLocation(){
        //判断是否有定位权限
        lm = (LocationManager) getApplication().getSystemService(LOCATION_SERVICE);
        boolean okstatue = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if(okstatue){
            Toast.makeText(this, "获取了权限", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "没有定位权限", Toast.LENGTH_SHORT).show();
        }
    }



    /**
     * 比较app版本大小
     * @param version1
     * @param version2
     * @return
     * @throws Exception
     */
    public  int compareVersion(String version1, String version2) throws Exception {
        if (version1 == null || version2 == null) {
            throw new Exception("compareVersion error:illegal params.");
        }
        String[] versionArray1 = version1.split("\\.");//注意此处为正则匹配，不能用"."；
        String[] versionArray2 = version2.split("\\.");
        int idx = 0;
        int minLength = Math.min(versionArray1.length, versionArray2.length);//取最小长度值
        int diff = 0;
        ////先比较长度    //再比较字符
     /*   while (idx < minLength
                && (versionArray1[idx].length() - versionArray2[idx].length()) == 0
                && (versionArray1[idx].compareTo(versionArray2[idx])) == 0) {

            diff = versionArray1[idx].length() - versionArray2[idx].length();
            diff = versionArray1[idx].compareTo(versionArray2[idx]);
            ++idx;
        }*/
     do{

         diff = versionArray1[idx].length() - versionArray2[idx].length();
         diff = versionArray1[idx].compareTo(versionArray2[idx]);

     }while(idx<minLength && diff==0);


        //如果已经分出大小，则直接返回，如果未分出大小，则再比较位数，有子版本的为大；
        diff = (diff != 0) ? diff : versionArray1.length - versionArray2.length;
        return diff;
    }


    /**
     * 获取手机通讯录
     */
    private void doGetRelationAll() {
        Observable.just(this)
                .map(new Func1<Context, List<RelationSubmitRequestEntity>>() {
                    @Override
                    public List<RelationSubmitRequestEntity> call(Context context) {
                        HashMap<String, String> map = ContactUtils.getAllContacts(context);
                        List<RelationSubmitRequestEntity> entityList = new ArrayList<>();
                        for (Map.Entry<String, String> entry : map.entrySet()) {
                            RelationSubmitRequestEntity entity = new RelationSubmitRequestEntity();
                            entity.setMobile(entry.getKey());
                            entity.setName(entry.getValue());
                            entityList.add(entity);
                        }
                        return entityList;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<RelationSubmitRequestEntity>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onNext(List<RelationSubmitRequestEntity> relationSubmitRequestEntities) {
                        if (relationSubmitRequestEntities != null && relationSubmitRequestEntities.size() > 0) {
                            contactsListJson = JSON.toJSONString(relationSubmitRequestEntities);
                            Log.d(TAG, "doGetRelationAll: " + "000000000" + contactsListJson);
                        }
                    }
                });


    }


    /**
     * 获取联系人
     */
    public void getContracts(){
        HashMap<String, String> map = ContactUtils.getAllContacts(context);
        List<RelationSubmitRequestEntity> entityList = new ArrayList<>();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            RelationSubmitRequestEntity entity = new RelationSubmitRequestEntity();
            entity.setMobile(entry.getKey());
            entity.setName(entry.getValue());
            entityList.add(entity);
        }
    }


    /**
     * uri转path
     * @param context
     * @param uri
     * @return
     */
    public static String getRealFilePath(final Context context, final Uri uri ) {
        if ( null == uri ) return null;
        final String scheme = uri.getScheme();
        String data = null;
        if ( scheme == null )
            data = uri.getPath();
        else if ( ContentResolver.SCHEME_FILE.equals( scheme ) ) {
            data = uri.getPath();
        } else if ( ContentResolver.SCHEME_CONTENT.equals( scheme ) ) {
            Cursor cursor = context.getContentResolver().query( uri, new String[] { MediaStore.Images.ImageColumns.DATA }, null, null, null );
            if ( null != cursor ) {
                if ( cursor.moveToFirst() ) {
                    int index = cursor.getColumnIndex( MediaStore.Images.ImageColumns.DATA );
                    if ( index > -1 ) {
                        data = cursor.getString( index );
                    }
                }
                cursor.close();
            }
        }
        return data;
    }



    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: ----Main");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: ----Main");
    }




    /**
     * 获取手机内存信息
     */
    public void getSDHelper(){

     /* Log.d("SDcard", "可用内存"+SDCardHelper.getSDCardAvailableSize());
        Log.d("SDcard", "获取SD卡的剩余空间大小"+SDCardHelper.getSDCardFreeSize());
        Log.d("SDcard", "全部内存"+SDCardHelper.getSDCardSize());
        Log.d("SDcard", "获取SD卡的根目录"+SDCardHelper.getSDCardBaseDir());
        Log.d("SDcard", "挂载"+SDCardHelper.isSDCardMounted());*/
    }




    @Subscribe
    public void onMessageEvent(MessageEvent event) {
        Toast.makeText(MainActivity.this, event.getMessage(), Toast.LENGTH_SHORT).show();
        if (event.getMessage().equals("HELLO")) {
            Log.d(TAG, "HELLO");
        }
    }

    @Subscribe
    public void handleSomethingElse(String event) {
//      doSomethingWith(event);
        Toast.makeText(this, "----做一些事情------", Toast.LENGTH_SHORT).show();
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        //必须调用该方法，防止内存泄漏
        ImmersionBar.with(this).destroy();
        //取消注册
        EventBus.getDefault().unregister(this);
    }


    //创建观察者
   /* Observer observer = new Observer() {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onNext(Object o) {

        }
    };*/


    //创建观察者2
   /* Subscriber subscriber = new Subscriber() {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onNext(Object o) {

        }
    };*/


    //被观察者创建
    /*Observable observable = Observable.create(new Observable.OnSubscribe() {
        @Override
        public void call(Object o) {

        }
    });*/




    /*Observable.create(new OnSubscribe() {
        @Override
        public void call(Subscribersub scriber){
            Drawable drawable = getTheme().getDrawable(drawableRes));
            subscriber.onNext(drawable);
            subscriber.onCompleted();
         })
         .subscribeOn(Schedulers.io())// 指定 subscribe() 发生在 IO 线程
         .observeOn(AndroidSchedulers.mainThread())// 指定 Subscriber 的回调发生在主线程
         .subscribe(new Observer() {
        @Override public void onNext (Drawable drawable)} {
            imageView.setImageDrawable(drawable);
        }
        @Override public void onCompleted () {

        }
        @Override
        public void onError (Throwable e) {
             Toast.makeText(activity, "Error!", Toast.LENGTH_SHORT).show();
         }
    });
*/

   /* Observable.just("images/logo.png")// 输入类型 String.map(newFunc1()
    {@Override
    public Bitmap call(String filePath){// 参数类型
     String return getBitmapFromPath(filePath);// 返回类型 Bitmap}}).subscribe(newAction1()
    {@Override
    public void call(Bitmap bitmap){// 参数类型 BitmapshowBitmap(bitmap);}});*/


//    Student[] students =new Student[]{"张三","李四","王五","赵六"};
   /* Subscriber subscriber = new Subscriber() {
    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onNext(Object o) {

    }
    };


     Observable.from(students).map(new Func1() {
     @Override
     public String call (Student student){
       return student.getName();
     }
     }) .subscribe(subscriber);*/

/*
    Observable.from(students).flatMap(new Func1>() {
    @Override
    public Observable call(Student student) {
       return Observable.from(student.getCourses());
     }
    }).subscribe(subscriber);*/


}
