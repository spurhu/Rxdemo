package com.example.huyahui.rxdemo;

import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HandlerActivity extends AppCompatActivity {


    @BindView(R.id.img)
    ImageView img;

    private String path="http://c.hiphotos.baidu.com/image/pic/item/77c6a7efce1b9d162f210013fedeb48f8d5464da.jpg";
    private String pathVideo="https://api.hongrenlian.cn/upload/image/hot/20180828/20180828135723.mp4";

    private static final String TAG = "HandlerActivity";

    //第一种方式：步骤一  实体类  也可以使用匿名类
   /* class MyHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
           //Glide.with(HandlerActivity.this).load(path).transition(new DrawableTransitionOptions().crossFade(500)).into(img);
            initVideo((String)msg.obj);
        }
    }*/

    //第一种方式 使用匿名类
    /*private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            initVideo((String)msg.obj);
        }
    };*/

     //第一种方式 使用post发送消息
    private Handler handler=new Handler();

    class NewHandelr extends Handler{

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch(msg.what){
                case 0:
                    Looper.prepare();
                    Toast.makeText(HandlerActivity.this, ""+msg.obj, Toast.LENGTH_SHORT).show();
                    Looper.loop();
                    break;
                case 1:
                    Looper.prepare();
                    Toast.makeText(HandlerActivity.this, ""+msg.obj, Toast.LENGTH_SHORT).show();
                    Looper.loop();
                    break;
            }
        }
    }

    private NewHandelr newHandelr;

    /**
     * 获取视频的第一帧缩略图
     * @param s
     */
    private void initVideo(String s) {
        RequestOptions options=new RequestOptions();
        options.diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(HandlerActivity.this).load(getNetVideoBitmap(s)).apply(options).into(img);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handler);
        ButterKnife.bind(this);

        initView();
    }


    /**
     * 初始化View
     */
    public void initView(){
        //第一种方式 使用实体类
        /*MyHandler handler=new MyHandler();
        Message message=new Message();
        message.what=1;
        message.obj=pathVideo;
        handler.sendMessage(message);*/

        ////第一种方式 使用匿名类
        /*Message message=new Message();
        message.what=1;//消息标识
        message.obj=pathVideo;//消息内存存放
        handler.sendMessage(message);*/

        //Handler第二种发送消息的方法 post
        /*handler.post(new Runnable() {
            @Override
            public void run() {
                initVideo(pathVideo);
            }
        });*/

        newHandelr=new NewHandelr();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Thread.sleep(1000);
                }catch (Exception e){
                    e.printStackTrace();
                }
                Message message=Message.obtain();
                message.what=0;
                message.obj="这是第一个线程";
                newHandelr.handleMessage(message);
            }
        }).start();

       /* new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Thread.sleep(3000);
                }catch (Exception e){
                    e.printStackTrace();
                }
                Message message=Message.obtain();
                message.what=1;
                message.obj="这是第二个线程";
                newHandelr.handleMessage(message);

            }
        }).start();*/

    }

    /**
     * 获取bitmap
     * @param videoUrl
     * @return
     */
    public Bitmap getNetVideoBitmap(String videoUrl) {
        Bitmap bitmap = null;
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            //根据url获取缩略图
            retriever.setDataSource(videoUrl, new HashMap());
            bitmap = retriever.getFrameAtTime();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } finally {
            retriever.release();
        }
        return bitmap;
    }



}
