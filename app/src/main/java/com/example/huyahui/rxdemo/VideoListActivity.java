package com.example.huyahui.rxdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.blankj.utilcode.util.ToastUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.ArrayList;
import java.util.List;
import adapter.GuideAdapter;

public class VideoListActivity extends AppCompatActivity {

    private SmartRefreshLayout smartRefreshLayout;
    private RecyclerView recyclerView;
    private GuideAdapter guideAdapter;
    private List<String> list=new ArrayList<>();

    private static final String TAG = "VideoListActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_list);
        initData();
        initRecycler();
    }

    public void initRecycler(){
        smartRefreshLayout=(SmartRefreshLayout)findViewById(R.id.smart) ;
        recyclerView=(RecyclerView)findViewById(R.id.video_recycler) ;
        recyclerView.setLayoutManager(new LinearLayoutManager(VideoListActivity.this));
        guideAdapter=new GuideAdapter(list,VideoListActivity.this);
        recyclerView.setAdapter(guideAdapter);
        guideAdapter.setOnItemListener(new GuideAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(int position) {
                switch (position) {
                    case 0:
                        startActivity(new Intent(VideoListActivity.this,VideoActivity.class));
                        break;
                    case 1:
                        startActivity(new Intent(VideoListActivity.this,VideoRecyclerViewActivity.class));
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    default:
                        break;
                }
            }
        });

        smartRefreshLayout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                Toast.makeText(VideoListActivity.this,"-----上拉加载-----",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                Toast.makeText(VideoListActivity.this,"-----下拉刷新-----",Toast.LENGTH_LONG).show();

            }
        });


    }



    /**
     * 初始化数据
     */
    public void initData() {
        list.add("NormalGSYVideoPlaye");
        list.add("RecyclerViewPlayer");
        list.add("00000000000");
        list.add("00000000000");
        list.add("00000000000");
        list.add("00000000000");
        list.add("00000000000");
        list.add("00000000000");
        list.add("00000000000");
        list.add("00000000000");
        list.add("00000000000");
        list.add("00000000000");
        list.add("00000000000");
        list.add("00000000000");
        list.add("00000000000");
        list.add("00000000000");
        list.add("00000000000");


    }







}
