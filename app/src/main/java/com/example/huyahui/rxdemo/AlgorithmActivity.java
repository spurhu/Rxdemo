package com.example.huyahui.rxdemo;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.CallLog.Calls;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AlgorithmActivity extends AppCompatActivity {

    private static final String TAG = "AlgorithmActivity";
    @BindView(R.id.edit)
    EditText edit;
    @BindView(R.id.btn_algor)
    Button btnAlgor;

    Button btn4;

    public static final int REQUEST_CODE=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_algorithm);
        ButterKnife.bind(this);
        btn4=(Button)findViewById(R.id.button4);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        int a = getCountStr("ABCDEFA", "A");
        Log.d(TAG, "onResume: 包含字符个数：" + a);
//        GetCallsInPhone();
    }

    /**
     * 思路：统一大小写，使用循环和charAt()判断
     * 计算字符串含有特定字符个数，不区分大小写
     */
    public int getCountStr(String input, String sub) {
        if (input.length() <= 0) {
            return 0;
        }
        int count = 0;
        //转为大写
        /*String inputString = input.toUpperCase();
        //返回指定位置的字符
        char subChar = sub.toUpperCase().charAt(0);*/

        String inputString = input.toLowerCase();
        //返回指定位置的字符
        char subChar = sub.toLowerCase().charAt(0);

        for (int i = 0; i < input.length(); i++) {
            if (subChar == inputString.charAt(i)) {
                count++;
            }

        }
        return count;
    }


    /**
     * @param s 输入随机字符串
     */
    public void strEight(StringBuffer s) {
        int e = 8;
        int l = 0;
        if (s != null && s.length() > 0) {
            l = s.length();
        }
        if (l < e) {
            for (int i = 0; i < e - l; i++) {
                s.append("0");
            }
            Log.d(TAG, "StrEight: 长度小于8," + s);
        } else if (l == e) {
            Log.d(TAG, "StrEight: 长度为8," + s);
        } else {
            int c = l / e;
            int d = l % e;
            //判断是否整除
            if (d != 0) {
                for (int i = 0; i < e - d; i++) {
                    s.append("0");
                }

                for (int i = 1; i <= c + 1; i++) {
                    Log.d(TAG, "StrEight: 不整除：" + s.substring((i - 1) * e, i * e) + "\n");
                }
            } else {
                for (int i = 1; i <= c; i++) {
                    Log.d(TAG, "StrEight: 整除：" + s.substring((i - 1) * e, i * e) + "\n");
                }
            }
        }

    }


    /**
     * 去掉重复的数字
     * @param num
     */
    public int deleteReat(int num){
        //防止出现空指针
        String str=String.valueOf(num);
        int len=str.length();
        //set:元素无放入顺序，元素不可重复
        Set<Character> set=new HashSet<>();
        StringBuilder sb=new StringBuilder();
        //倒序加入
        for (int i=len-1;i>=0;i--){
            //没有重复的,就可以添加(set特性),add()返回值是布尔，如果元素不存在则加入
            if(set.add(str.charAt(i))){
                sb.append(str.charAt(i));
            }
        }
        return Integer.parseInt(sb.toString());
    }


    /**
     * 字符串长度
     * @param s
     * @return
     */
    public int getCount(String s){
        return s.length();
    }



    @OnClick(R.id.btn_algor)
    public void onViewClicked() {
       /* String sd = edit.getText().toString();
          StringBuffer s = new StringBuffer(sd);
          strEight(s);*/

//        int i=deleteReat(65453212);

//        Log.d(TAG, "onViewClicked: "+ getCount("hhfdshofhso"));
//        getOrder(427505);
//        getOrder2("985784");

         int[] list = {36, 28, 45, 13, 67, 37, 18, 56};
        /*bubbleSort(list);
          for(int i=0;i<bubbleSort(list).length;i++){
            Log.d(TAG, "onViewClicked: "+bubbleSort(list)[i]);
        }*/

      /*  quickSort(list, 0, list.length - 1);
          display(list);*/


      /*  Intent intent=new Intent(AlgorithmActivity.this,ViewsActivity.class);
          intent.putExtra("BUNDLE_WEATHER_INFO", "万家乐");
          startActivityForResult(intent,100);*/

        /*for (int i = 0; i < chooseSort(list).length; i++) {
            Log.d(TAG, "onViewClicked: " + chooseSort(list)[i]);
        }*/

//        inserttionSort(list);

    }





    /**
     * 直接选择排序
     * @param list
     * @return
     */
    public int[] chooseSort(int[] list){

        for (int i = 0; i < list.length-1; i++) {
            int min = i;
            for (int j = i+1; j < list.length; j++) {
                if (list[j]>list[min]) {
                    min=j;
                }
            }
            if (min != 1) {
                swap(list, min, i);
            }
        }

        return list;
    }


    /**
     * 直接插入排序
     * @param list
     */
    public void inserttionSort(int[] list) {

      /*  for (int i = 0; i < list.length; i++) {
            int j = i;
            while (j > 0 && list[j - 1] < list[j - 1]) {
                //交换元素位置
                swap(list, j, j - 1);
                j--;
            }
        }
        //遍历
        display(list);*/

        for(int i = 1;i < list.length; i ++){
            //注意[0,i-1]都是有序的。如果待插入元素比arr[i-1]还大则无需再与[i-1]前面的元素进行比较了，反之则进入if语句
            if(list[i] < list[i-1]){
                int temp = list[i];
                int j;
                for(j = i-1; j >= 0 && list[j] > temp; j --){
                    //把比temp大或相等的元素全部往后移动一个位置
                    list[j+1] = list[j];
                }
                //把待排序的元素temp插入腾出位置的(j+1)
                list[j+1] = temp;
            }
        }
        //遍历
        display(list);

    }

   /*
    public class Main{
        public static void main(String [] args){
        System.out.println();

        }
    }*/

    /**
     * 转成数组，倒序输出
     * 颠倒顺序输出 方法一
     * @param i
     */
   public void getOrder(int i){
       String s=String.valueOf(i);
       StringBuffer sb=new StringBuffer("");
       char[] sa=s.toCharArray();
       int l=sa.length;
       for(int m=l-1;m>=0;m--){
           sb.append("-"+sa[m]);
       }
       Log.d(TAG, "getOrder: "+sb);
   }

    /**
     * 方法二
     * @param str
     */
   public void getOrder2(String str){

       if (str.length() == 1){
           System.out.print(str);
       }
       else{
           String subString1 = str.substring(0, str.length()-1);
           String subString2 = str.substring(str.length()-1);
           Log.d(TAG, "getOrder2: "+subString2);
           getOrder2 (subString1);
       }
   }


    /**
     * 冒泡排序
     * @param list
     */
    public int[] bubbleSort(int[] list) {
        int t;
        for (int i = 0; i < list.length; i++) {
            for (int j = 0; j < list.length - 1 - i; j++) {
                if(list[j]>list[j + 1]){
                    t = list[j];
                    list[j] = list[j + 1];
                    list[j + 1] = t;
                }
            }
        }
        return list;
    }


    /**
     * 快速排序
     * @param list
     */
    public void quickSort(int[] list,int left,int right){
        if(left<right){
            // 分割数组，找到分割点
            int point = partition(list, left, right);
            // 递归调用，对左子数组进行快速排序
            quickSort(list, left, point - 1);
            // 递归调用，对右子数组进行快速排序
            quickSort(list, point + 1, right);
        }
    }


    /**
     * 分割数组，找到分割点
     */
    public static int partition(int[] list, int left, int right) {
        // 用数组的第一个元素作为基准数
        int first = list[left];
        while (left < right) {
            while (left < right && list[right] >= first) {
                right--;
            }
            // 交换
            swap(list, left, right);

            while (left < right && list[left] <= first) {
                left++;
            }
            // 交换
            swap(list, left, right);
        }
        // 返回分割点所在的位置
        return left;
    }

    /**
     * 交换数组中两个位置的元素
     */
    public static void swap(int[] list, int left, int right) {
        int temp;
        if (list != null && list.length > 0) {
            temp = list[left];
            list[left] = list[right];
            list[right] = temp;
        }
    }



    /**
     * 遍历打印
     */
    public static void display(int[] list) {
        System.out.println("********展示开始********");
        if (list != null && list.length > 0) {
            for (int num : list) {
                Log.d(TAG, "display: "+num + "\n");
            }
        }
        Log.d(TAG, "********展示结束********");
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: "+data);
        Toast.makeText(this, "------回调传值------", Toast.LENGTH_SHORT).show();
        if (requestCode == RESULT_OK) {
            switch (requestCode) {
                case 1:
                    Bundle bundle = data.getBundleExtra("BUNDLE_WEATHER_INFO");
                    String msg = bundle.getString("CONTENT");
                    Log.d(TAG, "onActivityResult: " + msg);
                    break;
                case 2:
                    break;
                default:
                    Log.d(TAG, "onActivityResult: 没有默认值");
                    break;

            }
        }else{
            Log.d(TAG, "onActivityResult: ---------没有回调---------");
        }
    }



    /**
     * 获取通话记录
     */
    private void GetCallsInPhone() {
        String result = null;
        boolean hasRecord=true;
        Cursor cursor=null;
        try {
            cursor = getContentResolver().query(
                    Calls.CONTENT_URI,
                    new String[]{Calls.DURATION, Calls.TYPE, Calls.DATE,
                            Calls.NUMBER}, null, null, Calls.DEFAULT_SORT_ORDER);
            hasRecord = cursor.moveToFirst();
        } catch (SecurityException e) {
            Log.d(TAG, "GetCallsInPhone: ");
        }

        int count = 0;
        String strPhone = "";
        String date;
        while (hasRecord) {
            int type = cursor.getInt(cursor.getColumnIndex(Calls.TYPE));
            long duration = cursor.getLong(cursor.getColumnIndex(CallLog.Calls.DURATION));
            strPhone = cursor.getString(cursor.getColumnIndex(Calls.NUMBER));
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date d = new Date(Long.parseLong(cursor.getString(cursor.getColumnIndex(CallLog.Calls.DATE))));
            date = dateFormat.format(d);

            result = result + "phone :" + strPhone + ",";
            result = result + "date :" + date + ",";
            result = result + "time :" + duration + ",";

            switch (type) {
                case Calls.INCOMING_TYPE:
                    result = result + "type :呼入";
                    break;
                case Calls.OUTGOING_TYPE:
                    result = result + "type :呼出";
                default:
                    break;
            }
            result += "\n";
            count++;
            hasRecord = cursor.moveToNext();
        }
        Log.i(TAG, result);
        btnAlgor.setText(result);
    }






}
