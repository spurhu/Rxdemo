package com.example.huyahui.rxdemo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jakewharton.rxbinding.view.RxView;
import com.jakewharton.rxbinding.widget.RxTextView;
import com.jakewharton.rxbinding.widget.TextViewTextChangeEvent;

import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import util.AndroidJavaScript;
import util.DeviceUtils;
import util.TimeUtil;
import view.ObservableWebView;

/**
 * @author: huyahui
 * @date: 2018/3/2
 */
public class CustomWebviewActivity extends AppCompatActivity {

    private static final String TAG = "CustomWebviewActivity";

    @BindView(R.id.webview)
    ObservableWebView webview;
    @BindView(R.id.edit)
    EditText edit;
    @BindView(R.id.button)
    Button button;
    @BindView(R.id.text)
    TextView text;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.progressBar_web)
    ProgressBar progressBarWeb;

    private Subscription subscriptionEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_webview);
        ButterKnife.bind(this);
        initWebview();
        /*addEditListener();
          initView();*/
    }

    /**
     * 初始化视图
     */
    public void initView() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(image.getLayoutParams());
        WindowManager windowManager = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        int screenHeight = windowManager.getDefaultDisplay().getHeight();
        int screenWidth = windowManager.getDefaultDisplay().getWidth();

        int mIvLoanMarginButtom = (int) screenHeight * (618 - 553) / 618;
//        params.height= ViewGroup.LayoutParams.WRAP_CONTENT;
//        params.width= ViewGroup.LayoutParams.MATCH_PARENT;

//        params.width = (int) screenWidth * 70 / 375;
//        params.height = (int) screenHeight * 80 / 618;

        //子视图宽高是80*80
        params.width = (int) screenWidth * 80 / 375;
        params.height = (int) screenHeight * 80 / 618;


        params.setMargins(150, 120, 150, mIvLoanMarginButtom);

//        params.gravity= Gravity.CENTER;
//        params.weight=(float)1.0;

        image.setLayoutParams(params);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        initWindow();

    }

    private void initWindow() {
        WindowManager windowManager = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        int screenHeight = windowManager.getDefaultDisplay().getHeight();
        int screenWidth = windowManager.getDefaultDisplay().getWidth();
        //1920*1080
        Log.d(TAG, "onResume: 高和宽：" + screenHeight + "*" + screenWidth);
        int height = screenHeight - DeviceUtils.dpToPixel(this, 50) - getStatusBarHeight(this);
        Log.d(TAG, "onResume: height高：" + height);
        Log.d(TAG, "onResume: getLayoutParams：" + text.getLayoutParams());

        if (TimeUtil.isNight()) {
            Log.d(TAG, "onResume: 夜晚了");
        } else {
            Log.d(TAG, "onResume: 白天了");
        }
    }

    /**
     * 添加监听
     * editext
     */
    public void addEditListener() {
        /*subscription=RxView.clicks(button).subscribe(new Action1<Void>() {
             @Override
             public void call(Void aVoid) {
                 Log.d(TAG, "call: 点击了按钮");
             }
         });*/

       /*RxView.clicks(button)
               //仿抖，2秒钟只读取一个事件
               .throttleFirst(2,TimeUnit.SECONDS)
               .subscribe(new Action1<Void>() {
                   @Override
                   public void call(Void aVoid) {
                       Log.d(TAG, "call: click button");
                   }
               });*/

        RxView.longClicks(button).subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
                Log.d(TAG, "call: longclick");
            }
        });

        //点击按钮，多处响应
      /*  Observable<Object> observable = RxView.clicks(button).share();

        CompositeDisposable compositeDisposable = new CompositeDisposable();

        Disposable disposable1 = observable.subscribe(new Consumer<Object>() {
            @Override
            public void accept(Object o) throws Exception {
                Log.d(TAG, "disposable1, receive: " + o.toString());
            }
        });
        Disposable disposable2 = observable.subscribe(new Consumer<Object>() {
            @Override
            public void accept(Object o) throws Exception {
                Log.d(TAG, "disposable2, receive: " + o.toString());
            }
        });
        compositeDisposable.add(disposable1);
        compositeDisposable.add(disposable2);*/


        Observable<Void> observable = RxView.clicks(button).share();
        observable.observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Action1<Void>() {
                    @Override
                    public void call(Void aVoid) {
                        text.setText("*" + text.getText());
                    }
                })
                .buffer(observable.debounce(300, TimeUnit.MILLISECONDS))
                .map(new Func1<List<Void>, Integer>() {
                    @Override
                    public Integer call(List<Void> voids) {
                        return voids.size();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer integer) {
                        text.setText("*");
                        button.setText("" + integer + "连击");
                    }
                });


        //输入的监听
        subscriptionEdit = RxTextView.textChangeEvents(edit)
                //防抖时间 ,防止快速输入，只有当用户输入关键字后400毫秒才发射数据[说的直白点就是300毫秒后才会走后面的逻辑
                .debounce(300, TimeUnit.MILLISECONDS)
                //用于防止重复执行得到相同结果
                .distinctUntilChanged()
                //是跳过多少秒然后才开始将后面产生的数据提交给订阅者
                .skip(1)
                //假设你想察看一个的EditText输入文字时文本的变化（查看指定类型的数据）.EditText的原始文本类型是CharSequence的，
                //而你要获取倒序的字符串类型的文本，可是使用map
                .map(new Func1<TextViewTextChangeEvent, String>() {
                    @Override
                    public String call(TextViewTextChangeEvent textViewTextChangeEvent) {
                        return textViewTextChangeEvent.text().toString();
                    }
                })
                //用来过滤观测序列中我们不想要的值，只返回满足条件的值
                .filter(new Func1<String, Boolean>() {
                    @Override
                    public Boolean call(String s) {
                        //清空输入,刷新界面,
                        boolean isEmpty = TextUtils.isEmpty(s);
                        if (isEmpty) {
                            Log.d(TAG, "call: 清空输入...");
                        }
                        //只返回不为空的清空
                        return !isEmpty;
                    }
                })
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        Log.d(TAG, "call: subscribe");
                    }
                });


    }

    /**
     * 初始化webview  @SuppressLint:在调用js的地方要通过Lint告知编译器，此处要访问Js方法，且启用js访问，
     */
    @SuppressLint("SetJavaScriptEnabled")
    public void initWebview() {
        webview.loadUrl("file:///android_asset/huml.html");
        //不加此句，会跳到浏览器
        webview.setWebViewClient(new WebViewClient());
        webview.addJavascriptInterface(new AndroidJavaScript(this), "android");
//        webview.addJavascriptInterface(this, "android");
        //webview的滑动监听
        webview.setOnScrollChangeCallback(new ObservableWebView.OnScrollChangeCallback() {
            @Override
            public void onScroll(int l, int t, int oldl, int oldt) {
                if (webview.getScrollY() >= 50) {
                    Log.d(TAG, "onScroll: 滑动了--------");
                } else {
                    Log.d(TAG, "onScroll: -------------");
                }
            }
        });
        //支持App内部javascript交互
        webview.getSettings().setJavaScriptEnabled(true);
        // 支持缩放
        webview.getSettings().setSupportZoom(true);
        //是否开启本地DOM存  支持H5
        webview.getSettings().setDomStorageEnabled(true);
        //
        webview.getSettings().setDefaultTextEncodingName("gbk");

        //webview的回调方法
        webview.setWebViewClient(new WebViewClient() {
            //开始加载网页时调用  显示进度条
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Log.d(TAG, "onPageStarted: ------" + url);
//                showProgress(  true);
            }

            //加载结束时调用  隐藏进度条
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.d(TAG, "onPageFinished: -----" + url);
                //隐藏进度条
//                hideProgress();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                //加载出错时调用
//                webview.loadUrl("https://www.zhihu.com/");
                webview.loadUrl("http://www.12306.cn/mormhweb/");
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                //当接收到https错误时，会回调此函数，在其中可以做错误处理，当出现SSL错误时onReceivedError不会被回调
                Log.d(TAG, "onReceivedSslError: 12306");
            }


            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
                Log.d(TAG, "shouldInterceptRequest: "+request);
                return super.shouldInterceptRequest(view, request);
            }
        });

        //强制打开浏览器
        /*Uri uri=Uri.parse("https://cn.bing.com/");
        Intent intent=new Intent(Intent.ACTION_VIEW,uri);
        startActivity(intent);*/
        webview.setWebChromeClient(new WebChromeClient(){
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                //网页有弹框
                Log.d(TAG, "onJsAlert: 有弹框");
                return super.onJsAlert(view, url, message, result);
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                //加载进度
                Log.d(TAG, "onProgressChanged: "+newProgress);
            }
        });




    }


    /**
     * js调用java 的方法
     */
    @JavascriptInterface
    public void toastMsg() {
        Toast.makeText(this, "js调我的方法了,", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (subscriptionEdit != null && subscriptionEdit.isUnsubscribed()) {
            subscriptionEdit.unsubscribe();
        }

    }

    /**
     * 获取状态栏高度
     *
     * @param context
     * @return
     */
    private int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @OnClick(R.id.button3)
    public void onViewClicked() {
        Log.d(TAG, "onViewClicked: ********************");

        //源生调用JS的方法getjs()
        String url = "javascript:getjs(2,9)";
        webview.loadUrl(url);
        testEvauate(webview);
    }

    /**
     * android.onSumResult(result);
     * js的回调
     *
     * @param result
     */
    public void onSumResult(int result) {
        Log.d(TAG, "onSumREsult: 返回结果： " + result);
        Toast.makeText(this, "返回结果:" + result, Toast.LENGTH_SHORT).show();
    }


    /**
     * 获取js传来的值
     *
     * @param webView
     */
    private void testEvauate(WebView webView) {
        webView.evaluateJavascript("getjs()", new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String value) {
                Log.d(TAG, "onReceiveValue: " + value);
                Toast.makeText(CustomWebviewActivity.this, "返回结果：" + value, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private AlertDialog progressDialog;

    public void showProgress( boolean cancelable) {
        try {
            if (progressDialog == null) {
                final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this, R.style.MyDialog);
                View view = LayoutInflater.from(this).inflate(R.layout.progress_lottle, null);
                builder.setView(view);
                builder.setCancelable(cancelable);
                progressDialog = builder.create();
                progressDialog.show();
            } else {
                //设置为false，按返回键不能退出。默认为true。
                progressDialog.setCancelable(cancelable);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideProgress() {
        if (progressDialog != null) {
            try {
                progressDialog.dismiss();
                progressDialog = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
