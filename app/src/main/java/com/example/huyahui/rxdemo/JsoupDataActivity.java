package com.example.huyahui.rxdemo;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.blankj.utilcode.util.ToastUtils;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fragment.HomeFragment;
import fragment.MineFragment;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class JsoupDataActivity extends AppCompatActivity implements HomeFragment.MyListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tablayout)
    TabLayout tablayout;
    @BindView(R.id.appbarlayout)
    AppBarLayout appbarlayout;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.collapsingToolBarLayout)
    CollapsingToolbarLayout collapsingToolBarLayout;

    //private String url="http://xjh.haitou.cc/wh/uni-1/after/hold/page-1/";
    private String url = "http://www.dili360.com/gallery/";
    private String userAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:32.0) Gecko/    20100101 Firefox/32.0";


    private List<Fragment> fragments;
    private String[]  titles={"lift","right"};
    MyFragmentAdapter  adapter;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jsoup_data);
        ButterKnife.bind(this);

        new Thread(new Runnable() {
            @Override
            public void run() {
                getData();
            }
        }).start();

        fragments=new ArrayList<>();
        fragments.add(new MineFragment());
        fragments.add(new HomeFragment());


        adapter=new MyFragmentAdapter(getSupportFragmentManager());
        viewpager.setAdapter(adapter);
        tablayout.setupWithViewPager(viewpager);

        collapsingToolBarLayout.setTitle("CollapsingToolbarLayout");
        collapsingToolBarLayout.setAlpha(0.7f);
        collapsingToolBarLayout.setExpandedTitleColor(Color.GRAY);
        collapsingToolBarLayout.setCollapsedTitleTextColor(Color.RED);

//        appbarlayout.setScrollF

        collapsingToolBarLayout.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {

            }
        });

    }

    private static final String TAG = "JsoupDataActivity";

    //myFragment中的接口实现
    public void sendValue(String value) {
        ToastUtils.showLong(value);
        Log.d(TAG, "sendValue: "+value);
    }


    public void getData() {
        new Runnable() {
            @Override
            public void run() {
                try {
                    Connection conn = Jsoup.connect(url);
                    // 修改http包中的header,伪装成浏览器进行抓取
                    conn.header("User-Agent", userAgent);
                    Document doc = conn.get();
                    // 获取tbody元素下的所有tr元素
                    Element element = doc.select("wpr gallery-cate-list").first();
                  /* Elements elements = doc.select("tbody tr");
                   for(Element element : elements) {
                       //String companyName = element.getElementsByClass("text-success company pull-left").text();
                       String companyName = element.getElementsByTag("div").first().text();
                       String time = element.select("td.cxxt-time").text();
                       String address = element.select("td.text-ellipsis").first().text();

                       System.out.println("公司："+companyName);
                       System.out.println("宣讲时间："+time);
                       System.out.println("宣讲学校：华中科技大学");
                       System.out.println("具体地点："+address);
                       System.out.println("---------------------------------");
                   }*/
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }.run();

    }


    public void getData2() {
        Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(Subscriber<? super Object> subscriber) {
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Object>() {
                    @Override
                    public void onNext(Object o) {

                    }

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }


    @OnClick({R.id.toolbar, R.id.tablayout, R.id.appbarlayout, R.id.viewpager})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.toolbar:
                break;
            case R.id.tablayout:
                break;
            case R.id.appbarlayout:
                break;
            case R.id.viewpager:
                break;
        }
    }


    public class MyFragmentAdapter extends FragmentPagerAdapter{


        public MyFragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }

        @Override
        public float getPageWidth(int position) {
            return super.getPageWidth(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }
    }




}
