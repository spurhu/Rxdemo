package com.example.huyahui.rxdemo;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Application;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import view.ColorEvaluator;
import view.Point;
import view.PointEvaluator;
/**
 * @author: huyahui
 * @date: 2018/3/2
 * 动画
 */
public class AnimatorActivity extends AppCompatActivity {

    private static final String TAG = "AnimatorActivity";
    @BindView(R.id.progressBar2)
    ProgressBar progressBar2;
    @BindView(R.id.tv_text)
    TextView tvText;
    @BindView(R.id.button2)
    Button button2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animator);
        ButterKnife.bind(this);

    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: "+ getApplication());

    }

    /**
     * 初始化动画
     */
    public void initAnimation() {
        //用ofFloat()获取对象
        //三秒从0到5再到2
        //ValueAnimator anim = ValueAnimator.ofFloat(0f, 5f,2f);
        //三秒从0到5，会出现小数点
        //ValueAnimator anim = ValueAnimator.ofFloat(0f, 5f);
        //不出现小数点
        ValueAnimator anim = ValueAnimator.ofInt(0, 5);

        anim.setDuration(2000);
        //动画的监听
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
//                float currentValue = (float) animation.getAnimatedValue();
                int currentValue = (int) animation.getAnimatedValue();
                Log.d(TAG, "onAnimationUpdate: " + currentValue);
                tvText.setText("动画过程:" + currentValue);
            }
        });
        //动画延迟
        anim.setStartDelay(2000);
        //动画重复播放次数
        anim.setRepeatCount(3);
        //循环模式包括RESTART和REVERSE两种，分别表示重新播放和倒序播放
//        anim.setRepeatMode(ValueAnimator.RESTART);
        anim.setRepeatMode(ValueAnimator.REVERSE);

        if (anim.isStarted()) {
            Log.d(TAG, "initAnimation: 动画已经开始了");
        }

//        anim.start();

        if (anim.isRunning()) {
            Log.d(TAG, "initAnimation: isrunning");
        }


        //将一个TextView在5秒中内从常规变换成全透明，再从全透明变换成常规，就可以这样写：
//        ObjectAnimator animator = ObjectAnimator.ofFloat(tvText, "alpha", 1f, 0f, 1f);
        //旋转
//        ObjectAnimator animator = ObjectAnimator.ofFloat(tvText, "rotation", 1f, 360f);
        //先移出屏幕 再回来  500f右移，-500左移
//        float curTranslationX = tvText.getTranslationX();
//        ObjectAnimator animator = ObjectAnimator.ofFloat(tvText, "translationX", curTranslationX, 500f, curTranslationX);

        //将TextView在垂直方向上放大3倍再还原，
        //ObjectAnimator animator=ObjectAnimator.ofFloat(tvText,"scaleY",1f,3f,1f);

        //ofFloat 第二个参数：alpha，rotation，translationX，scaleY

        //让TextView先从屏幕外移动进屏幕，然后开始旋转360度，旋转的同时进行淡入淡出操作,最后在垂直方向上放大3倍再还原
        ObjectAnimator moveIn = ObjectAnimator.ofFloat(tvText, "translationX", -500f, 0f);
        ObjectAnimator rotate = ObjectAnimator.ofFloat(tvText, "rotation", 0f, 360f);
        ObjectAnimator fadeInOut = ObjectAnimator.ofFloat(tvText, "alpha", 1f, 0f, 1f);
        ObjectAnimator animator=ObjectAnimator.ofFloat(tvText,"scaleY",1f,3f,1f);
        AnimatorSet animSet = new AnimatorSet();
        animSet.play(rotate).after(3000).with(fadeInOut).after(moveIn).before(animator);
        animSet.setDuration(5000);
//        animSet.start();
        //动画的监听
        rotate.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                Log.d(TAG, "onAnimationStart: -----start-----");
                Toast.makeText(AnimatorActivity.this, "onAnimationStart", Toast.LENGTH_SHORT).show();
                super.onAnimationStart(animation);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                Log.d(TAG, "onAnimationCancel: ");
                Toast.makeText(AnimatorActivity.this, "onAnimationCancel", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                Log.d(TAG, "onAnimationEnd: ");
                Toast.makeText(AnimatorActivity.this, "onAnimationEnd", Toast.LENGTH_SHORT).show();
            }
        });

        fadeInOut.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        //动画文件的使用
        Animator animator1= AnimatorInflater.loadAnimator(this,R.animator.combination);
        animator1.setTarget(tvText);
//        animator1.start();

        //设置动画的时长
//        animator.setDuration(5000);
//        animator.start();

        /*Point p1=new Point(0,0);
        Point p2=new Point(300,1200);
        ValueAnimator valueAnimator=ValueAnimator.ofObject(new PointEvaluator(),p1,p2);//传入自定义方法做参数
        valueAnimator.setDuration(5000);
        valueAnimator.start();*/

       /* ObjectAnimator objectAnimator=ObjectAnimator.ofObject(tvText,"color",new ColorEvaluator(),"#0000FF","#FF0000");
        objectAnimator.setDuration(5000);
        objectAnimator.start();*/


    }


    @OnClick({R.id.tv_text, R.id.button2})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_text:
                break;
            case R.id.button2:
                initAnimation();
                Log.d(TAG, "onViewClicked: ------------");
                break;
            default:
                break;
        }
    }





}
