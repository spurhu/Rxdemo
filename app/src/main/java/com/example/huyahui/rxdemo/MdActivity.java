package com.example.huyahui.rxdemo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import adapter.CardAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import entity.Person;

public class MdActivity extends AppCompatActivity {

    @BindView(R.id.toobar)
    Toolbar toobar;
    @BindView(R.id.drwalayout)
    DrawerLayout drwalayout;
    @BindView(R.id.navigation_view)
    NavigationView navigationView;
    @BindView(R.id.recyclerview)
    RecyclerView recycler;
    @BindView(R.id.collapsingToolBarLayout)
    CollapsingToolbarLayout collapsingToolBarLayout;

    CardAdapter cardAdapter;
    List<Person> list;
    Person person=null;
    private String path="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1539413043972&di=95b68fa3bc2886550d90f9f5098fc65b&imgtype=0&src=http%3A%2F%2Fpic.90sjimg.com%2Fback_pic%2Fqk%2Fback_origin_pic%2F00%2F03%2F43%2F2ac2b10cf40bedde519be475dfe24461.jpg";
    private String path3="http://img0.dili360.com/ga/M02/02/18/wKgBzFQ2x9eAbZn8AATCSQolols997.jpg@!rw5";
    private String path2="http://a.hiphotos.baidu.com/image/pic/item/58ee3d6d55fbb2fb63e26464424a20a44723dcfe.jpg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_md);
        ButterKnife.bind(this);
        initToolbar();
        init();
        drawlayout();
        initRecyclew();
    }


    public void initRecyclew() {
        recycler.setLayoutManager(new LinearLayoutManager(MdActivity.this));
        list = new ArrayList<>();
        person=new Person(path3,"2013年4月，高圆圆通过壹基金为四川雅安灾区捐款2012年5月，高圆圆成为“关爱大龄女童”公益行动的志愿者");
        list.add(person);
        list.add(person);
        list.add(person);  list.add(person);
        list.add(person);
        list.add(person);
        cardAdapter = new CardAdapter(list, this);
        recycler.setAdapter(cardAdapter);
        cardAdapter.setOnItemClickListener(new CardAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(int position) {
                Toast.makeText(MdActivity.this, "----"+position, Toast.LENGTH_SHORT).show();
                cardAdapter.notifyItemChanged(0,new Person(path2,"2018-10-132:12"));

            }
        });

    }

    /**
     * 工具栏
     */
    public void initToolbar() {
        //设定按钮的图标
        toobar.setNavigationIcon(R.mipmap.icon_gcoding);
        //APP的图标。
        toobar.setLogo(R.mipmap.me_head);
        toobar.setSubtitle("副标题");
        toobar.setTitle("标题");
        //设定菜单各按钮的动作
        toobar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Toast.makeText(MdActivity.this, "setOnMenuItemClickListener", Toast.LENGTH_SHORT).show();
                return false;
            }
        });



    }

    private void init() {
        setSupportActionBar(toobar);



    }

    //菜单栏
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.option_1:
                Toast.makeText(MdActivity.this, "option_1", Toast.LENGTH_SHORT).show();
                break;
            case R.id.option_2:
                Toast.makeText(MdActivity.this, "option_2", Toast.LENGTH_SHORT).show();
                break;
            case R.id.option_3:
                Toast.makeText(MdActivity.this, "option_3", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
        return true;
    }

    /**
     * 侧滑布局
     */
    public void drawlayout() {
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,
                drwalayout,
                toobar,
                R.string.drawer_open,
                R.string.drawer_close
        ) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //打开
                Toast.makeText(MdActivity.this, "开启", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                //关闭
                Toast.makeText(MdActivity.this, "关闭", Toast.LENGTH_SHORT).show();
            }
        };
        //drwalayout和actionBarDrawerToggle的关联
        drwalayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        navigationView.setNavigationItemSelectedListener(new MyNavigationListener());
        //侧滑布局如果用NavigationView  点击后侧滑回去
        //如果用一般的布局，点击后不会侧滑回去

        collapsingToolBarLayout.setTitle("我是标题");


    }

    /**
     * 侧滑布局的点击监听
     */
    public class MyNavigationListener implements NavigationView.OnNavigationItemSelectedListener {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            drwalayout.closeDrawer(GravityCompat.START);
            switch (item.getItemId()) {
                case R.id.nav_blog:
                    Toast.makeText(MdActivity.this, "我的博客", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.nav_about:
                    Toast.makeText(MdActivity.this, "关于我", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.nav_sub1:
                    Toast.makeText(MdActivity.this, "副标题1", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.nav_sub2:
                    Toast.makeText(MdActivity.this, "副标题2", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    break;

            }
            return false;
        }
    }


}
