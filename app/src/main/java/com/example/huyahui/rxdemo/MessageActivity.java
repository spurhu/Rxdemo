package com.example.huyahui.rxdemo;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import base.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.TestDagger;

public class MessageActivity extends BaseActivity {

    @BindView(R.id.viewfilper)
    ViewFlipper viewfilper;
    @BindView(R.id.tvcount)
    TextView tvcount;
    @BindView(R.id.imageView)
    ImageView imageView;
    private String[] des = new String[]{"C语言从入门到精通", "Python学习之路。", "Spark SQL日志分析"};

    CountDownTimer countDownTimer;

    @Inject
    TestDagger testDagger;

    @Override
    protected int setLayoutId() {
        return R.layout.activity_message;
    }


    @Override
    protected void initData() {

        for (int i = 0; i < des.length; i++) {
            View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.filper_item, null);
            TextView tv = (TextView) view.findViewById(R.id.tvname);
            tv.setTextSize(19);
            tv.setGravity(Gravity.CENTER_VERTICAL);
            tv.setText(des[i]);
            TextView tvtime = (TextView) view.findViewById(R.id.tvtime);
            tvtime.setText("999999");
            viewfilper.addView(view);
        }

        //给ViewFlipper设置in/out的动画效果
        viewfilper.setInAnimation(MessageActivity.this, R.anim.put_up_in);
        viewfilper.setOutAnimation(MessageActivity.this, R.anim.put_up_out);

        //isFlipping： 用来判断View切换是否正在进行
        //setFilpInterval：设置View之间切换的时间间隔
        //startFlipping：使用上面设置的时间间隔来开始切换所有的View，切换会循环进行
        //stopFlipping: 停止View切换
        viewfilper.setFlipInterval(3000);
        viewfilper.startFlipping();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initTimer();
        countDown();

        imageView.setImageResource(R.mipmap.hall);
        String s=String.format(MessageActivity.this.getResources().getString(R.string.hello),"胡亚辉","牛牛",3);
        tvcount.setText(s);

//       TestDagger testDagger=new TestDagger();
//        Log.d("MessageActivity", ""+testDagger.toString());
    }


    //task应该在onDestory()中取消掉，否则可能发生崩溃
    Timer timer;

    /**
     * Timer和TimerTask
     * 定时器---方法一
     */
    public void initTimer() {
        timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("MessageActivity", "");
                        Toast.makeText(MessageActivity.this, "--------", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        //调度方法


        /**
         * 1,3只执行一次
         * 2，4每隔一段时间执行一次
         */

        //1.time为Date类型：在指定时间执行一次。
        /*timer.schedule(task, time);*/

        //2.firstTime为Date类型,period为long，表示从firstTime时刻开始，每隔period毫秒执行一次。
        /*Date day=new Date();
        timer.schedule(timerTask, day, 3000);*/

        //3.delay 为long类型：从现在起过delay毫秒执行一次。
        /*timer.schedule(timerTask, 3000);*/

        //4.delay为long,period为long：从现在起过delay毫秒以后，每隔period毫秒执行一次。
//        timer.schedule(timerTask, 2000, 2000);
    }


    /**
     * CountDownTimer
     * 每隔100毫秒执行onTick中的方法一次
     * 直到执行完10000/100次为止，最后会执行onFinish()中的方法
     */
    public void countDown() {
         countDownTimer = new CountDownTimer(10000, 200) {
            @Override
            public void onTick(long millisUntilFinished) {
//                tvcount.setText(millisUntilFinished + "--定时器");
            }

            @Override
            public void onFinish() {
                tvcount.setText("执行完了");
            }
        };

        countDownTimer.start();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
        }

        if (countDownTimer == null) {
            countDownTimer.cancel();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.imageView)
    public void onViewClicked() {
    }
}
