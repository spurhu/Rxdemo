package com.example.huyahui.rxdemo;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.alibaba.android.vlayout.DelegateAdapter;
import com.alibaba.android.vlayout.VirtualLayoutManager;
import com.alibaba.android.vlayout.layout.ColumnLayoutHelper;
import com.alibaba.android.vlayout.layout.FixLayoutHelper;
import com.alibaba.android.vlayout.layout.FloatLayoutHelper;
import com.alibaba.android.vlayout.layout.GridLayoutHelper;
import com.alibaba.android.vlayout.layout.LinearLayoutHelper;
import com.alibaba.android.vlayout.layout.OnePlusNLayoutHelper;
import com.alibaba.android.vlayout.layout.ScrollFixLayoutHelper;
import com.alibaba.android.vlayout.layout.SingleLayoutHelper;
import com.alibaba.android.vlayout.layout.StaggeredGridLayoutHelper;
import com.alibaba.android.vlayout.layout.StickyLayoutHelper;
import com.blankj.utilcode.util.ToastUtils;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import adapter.MyItemClickListener;
import adapter.VlayoutAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import util.MyToast;

/**
 * @author: huyahui
 * @date: 2018/3/15
 */

public class VlayoutActivity extends AppCompatActivity implements MyItemClickListener{

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.swiperefreshlayout)
    SwipeRefreshLayout swipeRefreshLayout;
    List<String> list;
    LinearLayoutHelper linearLayoutHelper;
    VlayoutAdapter Adapter_linearLayout,
                   Adapter_GridLayout,
                   Adapter_FixLayout,
                   Adapter_ScrollFixLayout,
                   Adapter_FloatLayout,
                   Adapter_ColumnLayout,
                   Adapter_SingleLayout,
                   Adapter_onePlusNLayoutt,
                   Adapter_StickyLayout,
                   Adapter_StaggeredGridLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vlayout);
        ButterKnife.bind(this);

        initRecyclerView();
    }

    private void initRecyclerView() {
        swipeRefreshLayout.setColorSchemeColors(Color.RED,Color.BLUE,Color.GREEN);

        /**
         *  1.初始化VirtualLayoutManager，并将VirtualLayoutManager绑定到recyclerView
         */
        VirtualLayoutManager virtualLayoutManager = new VirtualLayoutManager(this);
        recyclerview.setLayoutManager(virtualLayoutManager);

        /**
         *  2.设置缓存view个数(当视图中view的个数很多时，设置合理的缓存大小，防止来回滚动时重新创建 View)
         *  组件复用回收池
         */
        RecyclerView.RecycledViewPool recycledViewPool = new RecyclerView.RecycledViewPool();
        recyclerview.setRecycledViewPool(recycledViewPool);
        recycledViewPool.setMaxRecycledViews(0, 10);

        /**
         * 3.存放数据
         */
        initData();

        /**
         *
         * 4.根据数据列表,创建对应的LayoutHelper
         */
        initLinearLayout();

        /**
         设置吸边布局
         */
//        initStickLayout();


        /**
         设置可选固定布局
         */
        initScrollFixLayout();

        /**
         设置Grid布局
         */
//        initGridLayout();

        /**
         设置固定布局
         */
//        initFixLayout();

        /**
         设置浮动布局
         */
//        initFloatLayout();

        /**
         设置栏格布局
         */
//        initCloumnLayout();

        /**
         设置通栏布局
         */
//        initSingLayout();

        /**
         设置1拖N布局
         */
//        initOnePlusNLayout();

        /**
         设置瀑布流布局
         */
//        initStaggeredGridLayout();



        /**
         * 5.将生成的LayoutHelper 交给Adapter,并绑定到RecyclerView对象
         */
         //5.1 设置Adapter列表(同时也是设置LayoutHelper列表)
         List<DelegateAdapter.Adapter> adapters=new LinkedList<>();

        //5.2 将上述创建的Adapter对象放入到DelegateAdapter.Adapter列表
        adapters.add(Adapter_linearLayout);
//        adapters.add(Adapter_StickyLayout);
        adapters.add(Adapter_ScrollFixLayout);
//        adapters.add(Adapter_GridLayout);
//        adapters.add(Adapter_FixLayout);

//        adapters.add(Adapter_FloatLayout);
//        adapters.add(Adapter_ColumnLayout);
//        adapters.add(Adapter_SingleLayout);
//        adapters.add(Adapter_onePlusNLayoutt);
//        adapters.add(Adapter_StaggeredGridLayout);

        //5.3 创建DelegateAdapter对象 & 将layoutManager绑定到DelegateAdapter
        DelegateAdapter delegateAdapter=new DelegateAdapter(virtualLayoutManager);

        //5.4 将DelegateAdapter.Adapter列表绑定到DelegateAdapter
        delegateAdapter.setAdapters(adapters);

        //5.5  将delegateAdapter绑定到recyclerView
        recyclerview.setAdapter(delegateAdapter);


        /**
         * 6. 设置分割线 & Item之间的间隔
         */
        recyclerview.addItemDecoration(new DividerItemDecoration(this,virtualLayoutManager.getOrientation()));

        //需要自定义的DividerItemDecoration
        recyclerview.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.set(5,5,5,5);
            }
        });


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        List<String> headDatas = new ArrayList<String>();
                        for (int i = 0; i <3 ; i++) {

                            headDatas.add("Heard Item "+i);
                        }
                        Adapter_linearLayout.AddHeadItem(headDatas);

                        //刷新完成
                        swipeRefreshLayout.setRefreshing(false);
                        MyToast.showMessage(VlayoutActivity.this,"更新了 "+headDatas.size()+" 条目数据");
                        ToastUtils.setBgColor(getResources().getColor(R.color.gold));
                    }

                }, 2000);
            }

        });




    }


    /**
     * 7.实现点击事件
     *
     * @param view
     * @param position
     */
    @Override
    public void OnItemClick(View view, int position) {
        ToastUtils.showLong("点击了" + list.get(position) + "\n" + position + "行");
    }

    /**
     * 初始化数据
     */
    private void initData() {
        list = new ArrayList<>();
        for(int i=0;i<10;i++){
            list.add(""+i);
        }
    }



    /**
     * 初始化瀑布流布局
     */
    private void initStaggeredGridLayout() {
        // 创建对象
        StaggeredGridLayoutHelper staggeredGridLayoutHelper = new StaggeredGridLayoutHelper();

        // 公有属性
        // 设置布局里Item个数
//        staggeredGridLayoutHelper.setItemCount(40);
        // 设置LayoutHelper的子元素相对LayoutHelper边缘的距离
        staggeredGridLayoutHelper.setPadding(20, 20, 20, 20);
        // 设置LayoutHelper边缘相对父控件（即RecyclerView）的距离
        staggeredGridLayoutHelper.setMargin(20, 20, 20, 20);
        // 设置背景颜色
        staggeredGridLayoutHelper.setBgColor(Color.GRAY);
        // 设置设置布局内每行布局的宽与高的比
        staggeredGridLayoutHelper.setAspectRatio(3);

        // 特有属性
        // 设置控制瀑布流每行的Item数
        staggeredGridLayoutHelper.setLane(2);
        // 设置子元素之间的水平间距
        staggeredGridLayoutHelper.setHGap(20);
        // 设置子元素之间的垂直间距
        staggeredGridLayoutHelper.setVGap(15);

        Adapter_StaggeredGridLayout = new VlayoutAdapter(list,this, staggeredGridLayoutHelper,40) {
            // 设置需要展示的数据总数,此处设置是20

            // 通过重写onBindViewHolder()设置更加丰富的布局
            @Override
            public void onBindViewHolder(VlayoutHolder holder, int position) {
                super.onBindViewHolder(holder, position);

                ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,150 +position % 5 * 20);
                holder.itemView.setLayoutParams(layoutParams);

                // 为了展示效果,设置不同位置的背景色
                if (position > 10) {
                    holder.itemView.setBackgroundColor(0x66cc0000 + (position - 6) * 128);
                } else if (position % 2 == 0) {
                    holder.itemView.setBackgroundColor(0xaa22ff22);
                } else {
                    holder.itemView.setBackgroundColor(0xccff22ff);
                }

                // 为了展示效果,通过将布局的第一个数据设置为staggeredGrid
                if (position == 0) {
//                    holder.Text.setText("StaggeredGridLayout");
                }
            }
        };

        Adapter_StaggeredGridLayout.setMyItemClickListener(this);
        // 设置每个Item的点击事件
    }

    /**
     * 初始化一拖N布局
     */
    private void initOnePlusNLayout() {
        OnePlusNLayoutHelper onePlusNLayoutHelper = new OnePlusNLayoutHelper(5);
        // 在构造函数里传入显示的Item数
        // 最多是1拖4,即5个

        // 公共属性
        // 设置布局里Item个数
        onePlusNLayoutHelper.setItemCount(3);
        // 设置LayoutHelper的子元素相对LayoutHelper边缘的距离
        onePlusNLayoutHelper.setPadding(20, 20, 20, 20);
        // 设置LayoutHelper边缘相对父控件（即RecyclerView）的距离
        onePlusNLayoutHelper.setMargin(20, 20, 20, 20);
        // 设置背景颜色
        onePlusNLayoutHelper.setBgColor(Color.GRAY);
        // 设置设置布局内每行布局的宽与高的比
        onePlusNLayoutHelper.setAspectRatio(3);


        Adapter_onePlusNLayoutt = new VlayoutAdapter(list,this, onePlusNLayoutHelper,4) {
            // 设置需要展示的数据总数,此处设置是5,即1拖4
            // 为了展示效果,通过重写onBindViewHolder()将布局的第一个数据设置为onePlus
            @Override
            public void onBindViewHolder(VlayoutHolder holder, int position) {
                super.onBindViewHolder(holder, position);
                if (position == 0) {
//                    holder.Text.setText("onePlus");
                }
            }
        };

        Adapter_onePlusNLayoutt.setMyItemClickListener(this);
    }

    /**
     * 初始化通栏布局
     */
    private void initSingLayout() {
        SingleLayoutHelper singleLayoutHelper = new SingleLayoutHelper();

        // 公共属性
        // 设置布局里Item个数
        singleLayoutHelper.setItemCount(3);
        // 设置LayoutHelper的子元素相对LayoutHelper边缘的距离
        singleLayoutHelper.setPadding(20, 20, 20, 20);
        // 设置LayoutHelper边缘相对父控件（即RecyclerView）的距离
        singleLayoutHelper.setMargin(20, 20, 20, 20);
        // 设置背景颜色
        singleLayoutHelper.setBgColor(Color.GRAY);
        // 设置设置布局内每行布局的宽与高的比
        singleLayoutHelper.setAspectRatio(6);


        Adapter_SingleLayout = new VlayoutAdapter(list,this, singleLayoutHelper,3) {
            // 设置需要展示的数据总数,此处设置是1
            // 为了展示效果,通过重写onBindViewHolder()将布局的第一个数据设置为Single
            @Override
            public void onBindViewHolder(VlayoutHolder holder, int position) {
                super.onBindViewHolder(holder, position);
                if (position == 0) {
//                    holder.Text.setText("Single");
                }
            }
        };

        Adapter_SingleLayout.setMyItemClickListener(this);
        // 设置每个Item的点击事件
    }

    /**
     * 初始化栏格布局
     */
    private void initCloumnLayout() {
        ColumnLayoutHelper columnLayoutHelper = new ColumnLayoutHelper();
        // 创建对象

        // 公共属性
        // 设置布局里Item个数
        columnLayoutHelper.setItemCount(3);
        // 设置LayoutHelper的子元素相对LayoutHelper边缘的距离
        columnLayoutHelper.setPadding(20, 20, 20, 20);
        // 设置LayoutHelper边缘相对父控件（即RecyclerView）的距离
        columnLayoutHelper.setMargin(20, 20, 20, 20);
        // 设置背景颜色
        columnLayoutHelper.setBgColor(Color.YELLOW);
        // 设置设置布局内每行布局的宽与高的比
        columnLayoutHelper.setAspectRatio(6);


        // columnLayoutHelper特有属性
        // 设置该行每个Item占该行总宽度的比例
        columnLayoutHelper.setWeights(new float[]{30, 40, 30});

        Adapter_ColumnLayout = new VlayoutAdapter(list,this, columnLayoutHelper,30) {
            // 设置需要展示的数据总数,此处设置是3
            // 为了展示效果,通过重写onBindViewHolder()将布局的第一个数据设置为Column
            @Override
            public void onBindViewHolder(VlayoutHolder holder, int position) {
                super.onBindViewHolder(holder, position);
                if (position == 0) {
//                    holder.Text.setText("Column");
                }
            }
        };
        // 设置每个Item的点击事件
        Adapter_ColumnLayout.setMyItemClickListener(this);

    }

    /**
     * 初始化浮动布局
     */
    private void initFloatLayout() {
        FloatLayoutHelper floatLayoutHelper = new FloatLayoutHelper();
        // 创建FloatLayoutHelper对象

        // 公共属性
        // 设置布局里Item个数
        floatLayoutHelper.setItemCount(1);
        // 从设置Item数目的源码可以看出，一个FixLayoutHelper只能设置一个
        /*@Override
        public void setItemCount(int itemCount) {
            if (itemCount > 0) {
                super.setItemCount(1);
            } else {
                super.setItemCount(0);
            }
        }*/

        // 设置LayoutHelper的子元素相对LayoutHelper边缘的距离
        floatLayoutHelper.setPadding(20, 20, 20, 20);
        // 设置LayoutHelper边缘相对父控件（即RecyclerView）的距离
        floatLayoutHelper.setMargin(20, 20, 20, 20);
        // 设置背景颜色
        floatLayoutHelper.setBgColor(Color.GREEN);
        // 设置设置布局内每行布局的宽与高的比
        floatLayoutHelper.setAspectRatio(6);

        // floatLayoutHelper特有属性
        // 设置布局里Item的初始位置
        floatLayoutHelper.setDefaultLocation(300, 300);

        Adapter_FloatLayout = new VlayoutAdapter(list,this, floatLayoutHelper,1) {
            // 设置需要展示的数据总数,此处设置是1
            // 为了展示效果,通过重写onBindViewHolder()将布局的第一个数据设置为float
            @Override
            public void onBindViewHolder( VlayoutHolder holder, int position) {
                super.onBindViewHolder(holder, position);
                ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(500,1000);
                holder.itemView.setLayoutParams(layoutParams);
                holder.itemView.setBackgroundColor(Color.RED);

                if (position == 0) {
//                    holder.Text.setText("float");
                }
            }
        };

        Adapter_FloatLayout.setMyItemClickListener(this);
        // 设置每个Item的点击事件
    }

    /**
     * 初始化固定布局
     */
    private void initFixLayout() {
        FixLayoutHelper fixLayoutHelper = new FixLayoutHelper(FixLayoutHelper.TOP_LEFT,40,100);
        // 参数说明:
        // 参数1:设置吸边时的基准位置(alignType) - 有四个取值:TOP_LEFT(默认), TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT
        // 参数2:基准位置的偏移量x
        // 参数3:基准位置的偏移量y


        // 公共属性
        // 设置布局里Item个数
        fixLayoutHelper.setItemCount(1);
        // 从设置Item数目的源码可以看出，一个FixLayoutHelper只能设置一个
        /*@Override
        public void setItemCount(int itemCount) {
            if (itemCount > 0) {
                super.setItemCount(1);
            } else {
                super.setItemCount(0);
            }
        }*/
        // 设置LayoutHelper的子元素相对LayoutHelper边缘的距离
        fixLayoutHelper.setPadding(20, 20, 20, 20);
        // 设置LayoutHelper边缘相对父控件（即RecyclerView）的距离
        fixLayoutHelper.setMargin(20, 20, 20, 20);
        // 设置背景颜色
        fixLayoutHelper.setBgColor(Color.CYAN);
        // 设置设置布局内每行布局的宽与高的比
        fixLayoutHelper.setAspectRatio(6);

        // fixLayoutHelper特有属性
        // 设置吸边时的基准位置(alignType)
        fixLayoutHelper.setAlignType(FixLayoutHelper.TOP_LEFT);
        // 设置基准位置的横向偏移量X
        fixLayoutHelper.setX(30);
        // 设置基准位置的纵向偏移量Y
        fixLayoutHelper.setY(50);

        Adapter_FixLayout  = new VlayoutAdapter(list,this, fixLayoutHelper,1) {
            // 设置需要展示的数据总数,此处设置是1
            // 为了展示效果,通过重写onBindViewHolder()将布局的第一个数据设置为Fix
            @Override
            public void onBindViewHolder(VlayoutHolder holder, int position) {
                super.onBindViewHolder(holder, position);
                if (position == 0) {
//                    holder.Text.setText("Fix");
                }
            }
        };

        Adapter_FixLayout.setMyItemClickListener(this);
        // 设置每个Item的点击事件
    }

    /**
     * 初始化网格布局
     */
    private void initGridLayout() {
        GridLayoutHelper gridLayoutHelper = new GridLayoutHelper(3);
        // 在构造函数设置每行的网格个数

        // 公共属性
        // 设置布局里Item个数
        gridLayoutHelper.setItemCount(36);
        // 设置LayoutHelper的子元素相对LayoutHelper边缘的距离
        gridLayoutHelper.setPadding(0, 10, 0, 10);
        // 设置LayoutHelper边缘相对父控件（即RecyclerView）的距离
        gridLayoutHelper.setMargin(0, 10, 0, 10);
        // 设置背景颜色
        gridLayoutHelper.setBgColor(Color.DKGRAY);
        // 设置设置布局内每行布局的宽与高的比
        gridLayoutHelper.setAspectRatio(6);

        // gridLayoutHelper特有属性
        //设置每行中 每个网格宽度 占 每行总宽度 的比例
        gridLayoutHelper.setWeights(new float[]{40, 30, 30});
        // 控制子元素之间的垂直间距
        gridLayoutHelper.setVGap(20);
        // 控制子元素之间的水平间距
        gridLayoutHelper.setHGap(20);
        //是否自动填充空白区域
        gridLayoutHelper.setAutoExpand(false);
        // 设置每行多少个网格
        gridLayoutHelper.setSpanCount(3);
        // 通过自定义SpanSizeLookup来控制某个Item的占网格个数
        gridLayoutHelper.setSpanSizeLookup(new GridLayoutHelper.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position > 13) {
                    return 1;
                } else if (position > 9) {
                    return 3;
                    // 第7个位置后,每个Item占3个网格
                } else if (position > 5) {
                    return 2;
                    // 第7个位置后,每个Item占3个网格
                } else {
                    return 1;
                    // 第7个位置前,每个Item占2个网格
                }
            }
        });

        Adapter_GridLayout  = new VlayoutAdapter(list,this, gridLayoutHelper,36) {
            // 设置需要展示的数据总数,此处设置是8,即展示总数是8个,然后每行是4个(上面设置的)
            // 为了展示效果,通过重写onBindViewHolder()将布局的第一个数据设置为gridLayoutHelper
            @Override
            public void onBindViewHolder(VlayoutHolder holder, int position) {
                super.onBindViewHolder(holder, position);
                // 为了展示效果,将布局里不同位置的Item进行背景颜色设置
                if (position < 2) {
                    holder.itemView.setBackgroundColor(0x66cc0000 + (position - 6) * 128);
                } else if (position % 2 == 0) {
                    holder.itemView.setBackgroundColor(0xaa22ff22);
                } else {
                    holder.itemView.setBackgroundColor(0xccff22ff);
                }



                if (position == 0) {
//                    holder.Text.setText("Grid");
                }
            }
        };

        Adapter_GridLayout.setMyItemClickListener(this);
        // 设置每个Item的点击事件
    }

    /**
     * 初始化可选显示的固定布局
     */
    private void initScrollFixLayout() {
        ScrollFixLayoutHelper scrollFixLayoutHelper = new ScrollFixLayoutHelper(ScrollFixLayoutHelper.TOP_RIGHT,0,0);
        // 参数说明:
        // 参数1:设置吸边时的基准位置(alignType) - 有四个取值:TOP_LEFT(默认), TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT
        // 参数2:基准位置的偏移量x
        // 参数3:基准位置的偏移量y

        // 公共属性
        // 设置布局里Item个数
        scrollFixLayoutHelper.setItemCount(1);
        // 设置LayoutHelper的子元素相对LayoutHelper边缘的距离
        scrollFixLayoutHelper.setPadding(20, 20, 20, 20);
        // 设置LayoutHelper边缘相对父控件（即RecyclerView）的距离
        scrollFixLayoutHelper.setMargin(20, 20, 20, 20);
        // 设置背景颜色
        scrollFixLayoutHelper.setBgColor(Color.LTGRAY);
        // 设置设置布局内每行布局的宽与高的比
        scrollFixLayoutHelper.setAspectRatio(6);

        // fixLayoutHelper特有属性
        // 设置吸边时的基准位置(alignType)  TOP_LEFT(默认), TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT，
        scrollFixLayoutHelper.setAlignType(FixLayoutHelper.BOTTOM_LEFT);
        // 设置基准位置的横向偏移量X
        scrollFixLayoutHelper.setX(30);
        // 设置基准位置的纵向偏移量Y
        scrollFixLayoutHelper.setY(50);
        // 设置Item的显示模式
        scrollFixLayoutHelper.setShowType(ScrollFixLayoutHelper.SHOW_ON_LEAVE);


        Adapter_ScrollFixLayout  = new VlayoutAdapter(list,this, scrollFixLayoutHelper,1) {
            // 设置需要展示的数据总数,此处设置是1
            // 为了展示效果,通过重写onBindViewHolder()将布局的第一个数据设置为scrollFix
            @Override
            public void onBindViewHolder(VlayoutHolder holder, int position) {
                super.onBindViewHolder(holder, position);
                if (position == 0) {
//                    holder.Text.setText("scrollFix");
                }
            }
        };

        Adapter_ScrollFixLayout.setMyItemClickListener(this);
        // 设置每个Item的点击事件
    }

    /**
     * 如果看效果，要和linearLayout配合使用
     *
     * 初始化吸边布局
     * 布局说明：布局只有一个Item
     * 当它包含的组件处于屏幕可见范围内时，像正常的组件一样随页面滚动而滚动
     * 当组件将要被滑出屏幕返回的时候，可以吸到屏幕的顶部或者底部，实现一种吸住的效果
     */
    private void initStickLayout() {
        StickyLayoutHelper stickyLayoutHelper = new StickyLayoutHelper();

        // 公共属性
        // 设置布局里Item个数
        stickyLayoutHelper.setItemCount(3);
        // 设置LayoutHelper的子元素相对LayoutHelper边缘的距离
        stickyLayoutHelper.setPadding(20, 20, 0, 20);
        // 设置LayoutHelper边缘相对父控件（即RecyclerView）的距离
        stickyLayoutHelper.setMargin(20, 20, 20, 0);
        // 设置背景颜色
        stickyLayoutHelper.setBgColor(Color.MAGENTA);
        // 设置设置布局内每行布局的宽与高的比
        stickyLayoutHelper.setAspectRatio(3);

        // 特有属性
        // 当视图的位置在屏幕范围内时，视图会随页面滚动而滚动；当视图的位置滑出屏幕时，
        // StickyLayoutHelper会将视图固定在
        // 顶部（stickyStart = true）
        // 底部（stickyStart = false）
        stickyLayoutHelper.setStickyStart(true);
        // 设置吸边位置的偏移量
        stickyLayoutHelper.setOffset(150);

        Adapter_StickyLayout = new VlayoutAdapter(list,this, stickyLayoutHelper,1) {
            // 设置需要展示的数据总数,此处设置是1
            // 为了展示效果,通过重写onBindViewHolder()将布局的第一个数据设置为Stick
            @Override
            public void onBindViewHolder(VlayoutHolder holder, int position) {
                super.onBindViewHolder(holder, position);
                if (position == 0) {
//                    holder.Text.setText("Stick");
                  holder.itemView.setBackgroundColor(getResources().getColor(R.color.gold));
                }
            }
        };
        // 设置每个Item的点击事件
        Adapter_StickyLayout.setMyItemClickListener(this);
    }

    /**
     * 初始化线性布局
     */
    private void initLinearLayout() {
        linearLayoutHelper = new LinearLayoutHelper();
        //公共属性
        //设置布局里的item个数
        linearLayoutHelper.setItemCount(4);
        // 设置LayoutHelper的子元素相对LayoutHelper边缘的距离
        linearLayoutHelper.setPadding(20, 20, 20, 20);
        // 设置LayoutHelper边缘相对父控件（即RecyclerView）的距离
        linearLayoutHelper.setMargin(20, 20, 20, 20);
        // 设置背景颜色
        linearLayoutHelper.setBgColor(Color.LTGRAY);
        // 设置设置布局内每行布局的宽与高的比
        linearLayoutHelper.setAspectRatio(3);

        // linearLayoutHelper特有属性
        // 设置间隔高度
        // 设置的间隔会与RecyclerView的addItemDecoration（）添加的间隔叠加.
        linearLayoutHelper.setDividerHeight(10);
        //设置布局底部与下个布局的间隔
        linearLayoutHelper.setMarginBottom(100);


        //linearLayoutHelper：绑定对应的LayoutHelper
        //4:传入该布局要显示的数据个数
        Adapter_linearLayout = new VlayoutAdapter(list, this, linearLayoutHelper, 4) {
            @Override
            public void onBindViewHolder(VlayoutHolder holder, int position) {
                super.onBindViewHolder(holder, position);


                ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,200);
                holder.itemView.setLayoutParams(layoutParams);

                if (position==0){
                    holder.itemView.setBackgroundColor(getResources().getColor(R.color.blue));
                }

                //为了展示效果,将布局里不同位置的Item进行背景颜色设置
                if (position < 10 && position > 0) {
                    holder.itemView.setBackgroundColor(0x66cc0000 + (position - 6) * 128);
                } else if (position % 2 == 0) {
                    holder.itemView.setBackgroundColor(0xaa22ff22);
                } else {
                    holder.itemView.setBackgroundColor(0xccff22ff);
                }
            }
        };
        // 设置每个Item的点击事件
        Adapter_linearLayout.setMyItemClickListener(this);


        recyclerview.setOnScrollListener(new RecyclerView.OnScrollListener() {
            int lastVisibleItem;
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState==RecyclerView.SCROLL_STATE_IDLE&&lastVisibleItem+1==Adapter_linearLayout.getItemCount()){
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            List<String> footerDatas = new ArrayList<String>();
                            for (int i = 0; i< 3; i++) {
                                footerDatas.add("footer  item" + i);
                            }
                            Adapter_linearLayout.AddFooterItem(footerDatas);
                            MyToast.showMessage(VlayoutActivity.this,"更新了 "+footerDatas.size()+" 条目数据");
                        }
                    },2000);
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                //最后一个可见的ITEM
                lastVisibleItem=layoutManager.findLastVisibleItemPosition();
            }
        });

    }


    /**
     * 开始跳转
     *
     * @param context
     */
    public static void start(Context context) {
        Intent intent = new Intent(context, VlayoutActivity.class);
        context.startActivity(intent);
    }


}
