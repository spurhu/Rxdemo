package com.example.huyahui.rxdemo;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageView;

import com.github.chrisbanes.photoview.PhotoView;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImageActivity extends AppCompatActivity {

    @BindView(R.id.img_photo)
    PhotoView imgPhoto;
    @BindView(R.id.imageView4)
    ImageView imageView4;
    @BindView(R.id.btn_qrimg)
    Button btnQrimg;

    String url="https://testddxyh5.fcity.xin/share.html?channel=1&uid=3598795";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        ButterKnife.bind(this);

        getImgPhoto();
    }

    @NonNull
    private void getImgPhoto() {
        imgPhoto.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

    }


    @OnClick(R.id.btn_qrimg)
    public void onViewClicked() {

        Bitmap bitmap=generateBitmap(url,100,100);
        imageView4.setImageBitmap(bitmap);
    }




    /**
     * 生成二维码
     * @param content
     * @param width
     * @param height
     * @return
     */
    public Bitmap generateBitmap(String content, int width, int height) {
        QRCodeWriter writer = new QRCodeWriter();
        Map<EncodeHintType, String> hint = new HashMap<>();
        hint.put(EncodeHintType.CHARACTER_SET, "utf-8");
        try {
            BitMatrix encode = writer.encode(content, BarcodeFormat.QR_CODE, width, height, hint);
            int[] pixels = new int[width * height];
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    if (encode.get(j, i)) {
                        pixels[i * width + j] = 0x00000000;
                    } else {
                        pixels[i * width + j] = 0xffffffff;
                    }
                }
            }
            return Bitmap.createBitmap(pixels, 0, width, width, height, Bitmap.Config.RGB_565);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }




}
