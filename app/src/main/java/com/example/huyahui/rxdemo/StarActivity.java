package com.example.huyahui.rxdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.jakewharton.scalpel.ScalpelFrameLayout;

import javax.inject.Inject;

import entity.Person;

public class StarActivity extends AppCompatActivity {
    //http://blog.csdn.net/lisdye2/article/details/51942511#comments


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View mainView = getLayoutInflater().inflate(R.layout.activity_star,null);
        ScalpelFrameLayout scalpelFrameLayout = new ScalpelFrameLayout(this);
        scalpelFrameLayout.addView(mainView);
        scalpelFrameLayout.setLayerInteractionEnabled(true);//开启3d
        scalpelFrameLayout.setDrawIds(true);//显示id
        scalpelFrameLayout.getChromeColor();
        setContentView(scalpelFrameLayout);
//        initDagger();
    }

    /*public void initDagger(){
        //构造桥梁对象,
        MainComponent mainComponent=DaggerMainComponent.builder().mainModule(new MainModule()).build();
        //注入
        mainComponent.inject(this);
    }*/
}
