package com.example.huyahui.rxdemo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import tyrantgit.explosionfield.ExplosionField;
import view.DivergeViewSecond;

public class LikeAnimationActivity extends AppCompatActivity {


    @BindView(R.id.textView2)
    TextView textView2;
    @BindView(R.id.textView3)
    TextView textView3;
    @BindView(R.id.textView4)
    TextView textView4;
    @BindView(R.id.img2)
    ImageView img2;
    @BindView(R.id.progressBar)
    MaterialProgressBar progressBar;
    private ExplosionField explosionField;

    private DivergeViewSecond mDivergeView;
    private ImageView mImageView;
    private ImageView img;
    private List<Bitmap> mList = new ArrayList<Bitmap>();

    private List<Integer> integerList = new ArrayList<>();

    String path = "http://e.hiphotos.baidu.com/image/pic/item/72f082025aafa40fafb5fbc1a664034f78f019be.jpg";

    private FrameLayout fl;
    private ExplosionView exv1;
    private AnimationDrawable exa1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_like_animation);
        ButterKnife.bind(this);

        explosionField = ExplosionField.attach2Window(this);

        mImageView = (ImageView) findViewById(R.id.iv_start);
        img = (ImageView) findViewById(R.id.img);
        mDivergeView = (DivergeViewSecond) findViewById(R.id.divergeView);
        initView();


        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 开始执行动画...
                explosionField.explode(view);
                // ExplosionField.explode后，父布局中虽看不到ImageView，但ImageView所占据的位置还会响应事件.
                // 如果想屏蔽此ImageView出现，则：
                img.setVisibility(View.VISIBLE);
            }
        });

        //设置图片圆角角度
        //RoundedCorners roundedCorners=new RoundedCorners(6);

        //通过RequestOptions扩展功能
        //RequestOptions options= RequestOptions.centerCropTransform();

        //Glide.with(this).load(Uri.parse(path)).into(img2);


        // Glide.with(LikeAnimationActivity.this).load(path).into(img);


        progressBar.getCurrentDrawable();


    }


    private void initView() {
        mList = new ArrayList<>();
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star1, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star2, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star3, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star4, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star5, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star6, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star22, null)).getBitmap());
      /*  mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star7, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star8, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star9, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star10, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star11, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star12, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star13, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star14, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star15, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star16, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star17, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star18, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star19, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star20, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star21, null)).getBitmap());
        mList.add(((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.icon_star22, null)).getBitmap());*/
        /*mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIndex == 21) {
                    mIndex = 0;
                }
                mDivergeView.startDiverges(mIndex);
                //mIndex++;
                mIndex=mIndex+3;
            }
        });*/

        integerList.add(0);
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);
        integerList.add(4);
        integerList.add(5);
        integerList.add(6);
       /* integerList.add(7);
        integerList.add(8);
        integerList.add(9);
        integerList.add(10);
        integerList.add(11);
        integerList.add(12);
        integerList.add(13);
        integerList.add(14);
        integerList.add(15);
        integerList.add(16);
        integerList.add(17);
        integerList.add(18);
        integerList.add(19);
        integerList.add(20);
        integerList.add(21);*/


        /**
         * 点击事件的目的是  心形散落区域的控件点击事件
         */
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDivergeView.startDiverges(integerList);
            }
        });

        mDivergeView.post(new Runnable() {
            @Override
            public void run() {
                mDivergeView.setEndPoint(new PointF(mDivergeView.getMeasuredWidth() / 2, 0));
                mDivergeView.setDivergeViewProvider(new Provider());
            }
        });
    }

    @OnClick({R.id.textView2, R.id.textView3, R.id.textView4})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.textView2:
                Toast.makeText(this, "2", Toast.LENGTH_SHORT).show();



                break;
            case R.id.textView3:
                Toast.makeText(this, "3", Toast.LENGTH_SHORT).show();
                break;
            case R.id.textView4:
                Toast.makeText(this, "4", Toast.LENGTH_SHORT).show();
                break;
        }
    }


    class Provider implements DivergeViewSecond.DivergeViewProvider {
        @Override
        public Bitmap getBitmap(Object obj) {
            return mList == null ? null : mList.get((int) obj);
        }
    }



    class ExplosionView extends ImageView{

        public ExplosionView(Context context) {
            super(context);
        }
        //handle the location of the explosion
        public void setLocation(int top,int left){
            this.setFrame(left, top, left+40, top+40);
        }
    }



    class LayoutListener implements View.OnTouchListener {

        public boolean onTouch(View v, MotionEvent event) {
            //firstly， u have to stop the animation,if the animation
            //is starting ,u can not start it again!
            exv1.setVisibility(View.INVISIBLE);
            exa1.stop();
            float x = event.getX();
            float y = event.getY();
            exv1.setLocation((int)y-20, (int)x-20);
            exv1.setVisibility(View.VISIBLE);
            exa1.start();
            return false;
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mList != null) {
            mList.clear();
            mList = null;
        }
    }
}
