package com.example.huyahui.rxdemo;

import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;
import com.gyf.barlibrary.ImmersionBar;
import com.qmuiteam.qmui.util.QMUIDeviceHelper;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.util.QMUIPackageHelper;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.qmuiteam.qmui.widget.grouplist.QMUIGroupListView;
import org.greenrobot.eventbus.EventBus;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import activity.EditActivity;
import activity.SplashActivity;
import adapter.GuideAdapter;
import timber.log.Timber;
import util.MessageEvent;

public class QmuiActivity extends AppCompatActivity {

    QMUIGroupListView groupListView;
    private RecyclerView recyclerView;
    Window window;
    StringBuffer sb = new StringBuffer("");

    private GuideAdapter guideAdapter;
    private List<String> list = new ArrayList<>();
    protected ImmersionBar mImmersionBar;
    Context context;
    /**
     * 判断是否MediaPlayer是否释放的标志
     */
    private boolean isRelease = true;
    private static final String TAG = "LifeCycle";

    MediaPlayer player;
    SoundPool soundPool;
    int sound_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qmui);
        context = getApplicationContext();

        initView();
    }

    /**
     * 初始化view
     */
    private void initView() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        initImmersionBar();
        initQmui();
//      initGroupListView();
        initData();
        initRecycler();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        String time = format.format(Calendar.getInstance().getTime());
    }


    /**
     * 初始化Qmui框架，调用方法
     */
    public void initQmui() {
        window = getWindow();
//        QMUIStatusBarHelper.FlymeSetStatusBarLightMode(window, true);//状态栏字体颜色深色
//        QMUIStatusBarHelper.getStatusbarHeight(this);
        sb.append("状态栏高度:" + QMUIStatusBarHelper.getStatusbarHeight(this) + "\n");

//        QMUIStatusBarHelper.setStatusBarLightMode(this);
//        QMUIStatusBarHelper.supportTransclentStatusBar6();

        getInfo();
        getDisplay();
        getPackageInfo();

    }


    /**
     * 初始化状态栏
     * addTag  给上面参数打标记，以后可以通过标记恢复
     */
    public void initImmersionBar() {
        mImmersionBar = ImmersionBar.with(this);
        mImmersionBar.init();
        mImmersionBar.statusBarView(R.id.top_view)
                .navigationBarColor(R.color.colorPrimary)
                .fullScreen(true)
                .addTag("PicAndColor")
                .init();
    }


    /**
     * 页面跳转
     *
     * @param context
     */
    public static void start(Context context) {
        Intent intent = new Intent(context, QmuiActivity.class);
        context.startActivity(intent);
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: -------Qmui");
    }


    @Override
    protected void onResume() {
        super.onResume();
        Timber.tag("gogogo");
        Timber.d("Activity Created");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mImmersionBar != null) {
            mImmersionBar.destroy();  //在BaseActivity里销毁
        }
        //释放资源
        if (player != null) {
            player.release();
        }
    }


    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    /**
     * 初始化recyclerview
     */
    public void initRecycler() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(QmuiActivity.this));
        guideAdapter = new GuideAdapter(list, QmuiActivity.this);
        recyclerView.setAdapter(guideAdapter);
        guideAdapter.setOnItemListener(new GuideAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(int position) {
                switch (position) {
                    case 0:
                        DialogActivity.start(QmuiActivity.this);
                        EventBus.getDefault().post(new MessageEvent("HELLO"));
                        break;
                    case 1:
                        startActivity(new Intent(QmuiActivity.this, SplashActivity.class));
//                         startActivity(new Intent(QmuiActivity.this, EditActivity.class));
                        break;
                    case 2:
                        VlayoutActivity.start(QmuiActivity.this);
                        break;
                    case 3:
                        startActivity(new Intent(QmuiActivity.this, CustomViewActivity.class));
                        break;
                    case 4:
                        startActivity(new Intent(QmuiActivity.this, MessageActivity.class));
                        break;
                    case 5:
                        startActivity(new Intent(QmuiActivity.this, StarActivity.class));
                        break;
                    case 6:
                        startActivity(new Intent(QmuiActivity.this, AnimatorActivity.class));
                        break;
                    case 7:
                        startActivity(new Intent(QmuiActivity.this, AlgorithmActivity.class));
                        break;
                    case 8:
                        startActivity(new Intent(QmuiActivity.this, MdActivity.class));
                        break;
                    case 9:
                        startActivity(new Intent(QmuiActivity.this, VideoListActivity.class));
                        break;
                    case 10:
                        startActivity(new Intent(QmuiActivity.this, CustomWebviewActivity.class));
                        break;
                    case 11:
                        startActivity(new Intent(QmuiActivity.this, ViewsActivity.class));
                        break;
                    case 12:
                        startActivity(new Intent(QmuiActivity.this, UnitTestActivity.class));
                        break;
                    case 13:
                        startActivity(new Intent(QmuiActivity.this, KeyboardActivity.class));
                        break;
                    case 14:
                        startActivity(new Intent(QmuiActivity.this, ImageActivity.class));
                        break;
                    case 15:
                        startActivity(new Intent(QmuiActivity.this, ExpandRecyclerViewActivity.class));
                        break;
                    case 16:
                        startActivity(new Intent(QmuiActivity.this, TransfereeActivity.class));
                        break;
                    case 17:
                        startActivity(new Intent(QmuiActivity.this, LikeAnimationActivity.class));
                        break;
                    case 18:
                        startActivity(new Intent(QmuiActivity.this, HandlerActivity.class));
                        break;
                    case 19:
                        startActivity(new Intent(QmuiActivity.this, MulImgSelectActivity.class));
                        break;
                     case 20:
                        startActivity(new Intent(QmuiActivity.this, JsoupDataActivity.class));
                        break;
                    case 21:
                        startActivity(new Intent(QmuiActivity.this, RecyclerViewActivity.class));
                        break;
                    default:
                        break;
                }
            }
        });


    }


    /**
     * 初始化列表数据
     */
    public void initData() {
        list.add("腾讯QMUI---各种对话框使用"); // https://qmuiteam.com/android
        list.add("EditActivity");
        list.add("阿里---Vlayout布局框架"); // https://www.jianshu.com/p/6b658c8802d1
        list.add("自定义View基础");
        list.add("Timer & filper");

        list.add("3D展示---调到StarActivity"); // https://www.jianshu.com/p/8ea577c60865
        list.add("动画AnimatorActivity");
        list.add("算法小程序");
        list.add("Material Design"); //
        list.add("视频开发"); // https://www.jianshu.com/p/49831e5e2cd6

        list.add("自定义的Webview & RXbind");
        list.add("View的事件分发和权限");
        list.add("As单元测试和view测试");
        list.add("上弹键盘输入框");
        list.add("图片放缩&生成二维码");

        list.add("折叠展开的RecyclerView");
        list.add("防微信朋友圈 图片九宫格");
        list.add("点赞动画 & ProgressBar");
        list.add("Hnadler机制");
        list.add("防微信多选图片 ");
        list.add("JsoupDataActivity ");

        list.add("RecyclerViewActivity ");
    }


    /**
     * 腾讯的工具
     * 获取设备信息的工具类
     */
    public void getInfo() {

        if (QMUIDeviceHelper.isTablet(this)) {
            Log.d("QmuiActivity", "是平板");
            sb.append("是平板" + "\n");
        } else if (QMUIDeviceHelper.isMeizu()) {
            Log.d("QmuiActivity", "是flyme");
            sb.append("是flyme" + "\n");
        } else if (QMUIDeviceHelper.isFloatWindowOpAllowed(this)) {
            Log.d("QmuiActivity", "isFloatWindowOpAllowed");
            sb.append("是isFloatWindowOpAllowede" + "\n");
        } else if (QMUIDeviceHelper.isFlymeVersionHigher5_2_4()) {
            Log.d("QmuiActivity", "isFlymeVersionHigher5_2_4");
            sb.append("是isFlymeVersionHigher5_2_4" + "\n");
        }

    }


    /**
     * 设备屏幕相关信息
     */
    public void getDisplay() {

        int sw = QMUIDisplayHelper.getScreenWidth(this);
        sb.append("屏幕宽度:" + sw + "\n");
        int sh = QMUIDisplayHelper.getActionBarHeight(this);
        sb.append("导航栏:" + sh + "\n");

    }

    /**
     * 安装包的信息
     */
    public void getPackageInfo() {
        String info = QMUIPackageHelper.getAppVersion(this);
        sb.append("getAppVersion:" + info + "\n");
        String info2 = QMUIPackageHelper.getMajorMinorVersion(this);
        sb.append("getMajorMinorVersion:" + info2 + "\n");
        Log.d("QmuiActivity", "" + sb);
    }


    /**
     * 金币声音
     */
    public void sound() {
        Toast.makeText(this, "music", Toast.LENGTH_SHORT).show();
        //重载
        if (isRelease) {
            player = MediaPlayer.create(this, R.raw.collect_gold);//本地
            isRelease = false;
        }
        player.start();

    }

    /**
     * 适合播放声音短，文件小
     * 可以同时播放多种音频
     * 消耗资源较小
     */
    public void playSound(int rawId) {
        SoundPool soundPool;
        if (Build.VERSION.SDK_INT >= 21) {
            SoundPool.Builder builder = new SoundPool.Builder();
            //传入音频的数量
            builder.setMaxStreams(1);
            //AudioAttributes是一个封装音频各种属性的类
            AudioAttributes.Builder attrBuilder = new AudioAttributes.Builder();
            //设置音频流的合适属性
            attrBuilder.setLegacyStreamType(AudioManager.STREAM_MUSIC);
            builder.setAudioAttributes(attrBuilder.build());
            soundPool = builder.build();
        } else {
            //第一个参数是可以支持的声音数量，第二个是声音类型，第三个是声音品质
            soundPool = new SoundPool(1, AudioManager.STREAM_SYSTEM, 5);
        }
        //第一个参数Context,第二个参数资源Id，第三个参数优先级
        soundPool.load(context, rawId, 1);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                soundPool.play(1, 1, 1, 0, 0, 1);
            }
        });
        //第一个参数id，即传入池中的顺序，第二个和第三个参数为左右声道，第四个参数为优先级，第五个是否循环播放，0不循环，-1循环
        //最后一个参数播放比率，范围0.5到2，通常为1表示正常播放
//        soundPool.play(1, 1, 1, 0, 0, 1);
        //回收Pool中的资源
        //soundPool.release();

    }


    /**
     * 加载音效
     */
    @SuppressWarnings("deprecation")
    private void loadSound() {
        soundPool = new SoundPool(5, AudioManager.STREAM_SYSTEM, 1);
        new Thread() {
            @Override
            public void run() {
                sound_id = soundPool.load(QmuiActivity.this, R.raw.collect_gold, 1);
            }
        }.start();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
