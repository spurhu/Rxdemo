package com.example.huyahui.rxdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import view.MyView;
/**
 * @author: huyahui
 * @date: 2018/3/12
 */
public class CustomViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_view);
        FrameLayout frameLayout=(FrameLayout)findViewById(R.id.frame_content);
        frameLayout.addView(new MyView(CustomViewActivity.this));
    }
}
