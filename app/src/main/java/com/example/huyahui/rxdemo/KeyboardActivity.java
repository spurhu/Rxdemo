package com.example.huyahui.rxdemo;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;

import activity.EditActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Demo  class
 *
 * @author: huyahui
 * @date: 2018/1/20
 */
public class KeyboardActivity extends AppCompatActivity {

    @BindView(R.id.imageView2)
    ImageView imageView2;
    @BindView(R.id.tvname)
    TextView tvname;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.linear_parent)
    LinearLayout linear_parent;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.tv_vercodelogin)
    TextView tvVercodelogin;
    @BindView(R.id.scrollview)
    ScrollView scrollview;
    @BindView(R.id.rela_title)
    RelativeLayout relaTitle;
    @BindView(R.id.edit_password)
    EditText editPassword;

    private int screenHeight;
    private int keyHeight;
    private float scale = 0.5f; //logo缩放比例

    public static final String EDIT_MOBILE = "MOBILE";
    private String edit = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keyboard);
        ButterKnife.bind(this);
        initView();
    }


    public void initView() {
        //获取屏幕高度
        screenHeight = getResources().getDisplayMetrics().heightPixels;
        //弹起高度为屏幕高度的1/3
        keyHeight = screenHeight / 3;
        //拉钩键盘上顶布局https://yq.aliyun.com/articles/180175
        scrollview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        scrollview.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (oldBottom != 0 && bottom != 0 && (oldBottom - bottom > keyHeight)) {
                    Log.e("wenzhihao", keyHeight + "up------>" + (oldBottom - bottom));

                    int dist = linear_parent.getBottom() - bottom;
                    if (dist > 0) {
                        ObjectAnimator mAnimatorTranslateY = ObjectAnimator.ofFloat(linear_parent, "translationY", 0.0f, -dist);
                        mAnimatorTranslateY.setDuration(300);
                        mAnimatorTranslateY.setInterpolator(new LinearInterpolator());
                        mAnimatorTranslateY.start();
                        zoomIn(imageView2, dist);
//                      imageView2.setVisibility(View.INVISIBLE);
                        tvname.setText("我是好人啊");
                    }

                } else if (oldBottom != 0 && bottom != 0 && (bottom - oldBottom > keyHeight)) {
                    Log.e("wenzhihao", "down------>" + (bottom - oldBottom));

                    if ((linear_parent.getBottom() - oldBottom) > 0) {
                        ObjectAnimator mAnimatorTranslateY = ObjectAnimator.ofFloat(linear_parent, "translationY", linear_parent.getTranslationY(), 0);
                        mAnimatorTranslateY.setDuration(300);
                        mAnimatorTranslateY.setInterpolator(new LinearInterpolator());
                        mAnimatorTranslateY.start();
                        imageView2.setVisibility(View.VISIBLE);
                        tvname.setVisibility(View.INVISIBLE);
                        zoomOut(imageView2);
                    }
                }
            }
        });

        tv_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastUtils.showLong("0000000时间");
            }
        });

        edit=getIntent().getStringExtra(EDIT_MOBILE);
        if(!TextUtils.isEmpty(edit)){
            editPassword.setText(edit);
        }
        editPassword.addTextChangedListener(textWatcher);

    }


    @OnClick(R.id.btn_login)
    public void onViewClicked() {
        edit =editPassword.getText().toString().trim();
        Intent intent = new Intent(KeyboardActivity.this, EditActivity.class);
        intent.putExtra(EDIT_MOBILE, edit);
        startActivity(intent);

    }


    /**
     *
     */
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            edit =editPassword.getText().toString().trim();
            Log.d("edit", "----editStr---" + s.toString());
            Log.d("edit", "----editPwd---" + editPassword.getText().toString().trim());
        }
    };


    /**
     * 缩小
     *
     * @param view
     */
    public void zoomIn(final View view, float dist) {
        view.setPivotY(view.getHeight());
        view.setPivotX(view.getWidth() / 2);
        AnimatorSet mAnimatorSet = new AnimatorSet();
        ObjectAnimator mAnimatorScaleX = ObjectAnimator.ofFloat(view, "scaleX", 1.0f, scale);
        ObjectAnimator mAnimatorScaleY = ObjectAnimator.ofFloat(view, "scaleY", 1.0f, scale);
        ObjectAnimator mAnimatorTranslateY = ObjectAnimator.ofFloat(view, "translationY", 0.0f, -dist);

        mAnimatorSet.play(mAnimatorTranslateY).with(mAnimatorScaleX);
        mAnimatorSet.play(mAnimatorScaleX).with(mAnimatorScaleY);
        mAnimatorSet.setDuration(300);
        mAnimatorSet.start();
    }


    /**
     * f放大
     *
     * @param view
     */
    public void zoomOut(final View view) {
        view.setPivotY(view.getHeight());
        view.setPivotX(view.getWidth() / 2);
        AnimatorSet mAnimatorSet = new AnimatorSet();

        ObjectAnimator mAnimatorScaleX = ObjectAnimator.ofFloat(view, "scaleX", scale, 1.0f);
        ObjectAnimator mAnimatorScaleY = ObjectAnimator.ofFloat(view, "scaleY", scale, 1.0f);
        ObjectAnimator mAnimatorTranslateY = ObjectAnimator.ofFloat(view, "translationY", view.getTranslationY(), 0);

        mAnimatorSet.play(mAnimatorTranslateY).with(mAnimatorScaleX);
        mAnimatorSet.play(mAnimatorScaleX).with(mAnimatorScaleY);
        mAnimatorSet.setDuration(300);
        mAnimatorSet.start();
    }
}
