package com.example.huyahui.rxdemo;

import android.annotation.TargetApi;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Transition;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.blankj.utilcode.util.ToastUtils;
import com.shuyu.gsyvideoplayer.utils.OrientationUtils;
import com.shuyu.gsyvideoplayer.video.NormalGSYVideoPlayer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import video.bean.OnTransitionListener;
import video.bean.SwitchVideoModel;

public class VideoActivity extends AppCompatActivity {

    public final static String IMG_TRANSITION = "IMG_TRANSITION";
    public final static String TRANSITION = "TRANSITION";

    private NormalGSYVideoPlayer normalGSYVideoPlayer;
    //重力旋转
    OrientationUtils orientationUtils;

    private boolean isTransition;
    private Transition transition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        normalGSYVideoPlayer=(NormalGSYVideoPlayer)findViewById(R.id.video_player);
        isTransition = getIntent().getBooleanExtra(TRANSITION, false);
        init();
    }

    /**
     * 初始化视频播放
     */
    public void init(){
//        String url="http://data.vod.itc.cn/?rb=1&key=jbZhEJhlqlUN-Wj_HEI8BjaVqKNFvDrn&prod=flash&pt=1&new=/193/45/4DIDEO2zpZHO9ZN9fnalR6.mp4";
        String url="http://data.vod.itc.cn/?rb=1&key=jbZhEJhlqlUN-Wj_HEI8BjaVqKNFvDrn&prod=flash&pt=1&new=/190/249/qCvmtO48otxI5Y8T1RNvqF.mp4";

        //借用了jjdxm_ijkplayer的URL
        String source1 = "http://9890.vod.myqcloud.com/9890_4e292f9a3dd011e6b4078980237cc3d3.f20.mp4";
        String name = "普通";
        SwitchVideoModel switchVideoModel = new SwitchVideoModel(source1,name);

        String source2 = "http://9890.vod.myqcloud.com/9890_4e292f9a3dd011e6b4078980237cc3d3.f30.mp4";
        String name2 = "清晰";
        SwitchVideoModel switchVideoModel2 = new SwitchVideoModel(source2,name2);

        List<SwitchVideoModel> list = new ArrayList<>();
        list.add(switchVideoModel);
        list.add(switchVideoModel2);
        //视频地址
        normalGSYVideoPlayer.setUp(url,true,"测试视频");
        //视频封面
        ImageView imageView=new ImageView(this);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        //imageView.setImageResource(R.mipmap.sun);

        getNetVideoBitmap(imageView,source1);

        normalGSYVideoPlayer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
               switch(motionEvent.getAction()){
                   case MotionEvent.ACTION_DOWN:
                       ToastUtils.showLong("DOWN");
                       break;
                   case MotionEvent.ACTION_UP:
                       Toast.makeText(VideoActivity.this, "UPUPUP", Toast.LENGTH_SHORT).show();
                       break;

               }
                return true;

            }
        });

        normalGSYVideoPlayer.setThumbImageView(imageView);
        //title可见
        normalGSYVideoPlayer.getTitleTextView().setVisibility(View.VISIBLE);

//        normalGSYVideoPlayer.setShowPauseCover(false);

        //快放
//        normalGSYVideoPlayer.setSpeed(2f);

        //显示返回键
        normalGSYVideoPlayer.getBackButton().setVisibility(View.VISIBLE);
        orientationUtils=new OrientationUtils(this,normalGSYVideoPlayer);

        //返回功能
        normalGSYVideoPlayer.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //全屏
        //横竖屏切换时 在主文件设置activity属性，否则视频将从新开始播放
        normalGSYVideoPlayer.getFullscreenButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orientationUtils.resolveByClick();
            }
        });

        //过渡动画
        initTransition();

    }


    //截取视频一帧 作为缩略图
    public Bitmap getNetVideoBitmap(ImageView imageView, String videoUrl) {
        Bitmap bitmap = null;
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            //根据url获取缩略图
            retriever.setDataSource(videoUrl, new HashMap());
            bitmap = retriever.getFrameAtTime();
            imageView.setImageBitmap(bitmap);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } finally {
            retriever.release();
        }
        return bitmap;
    }


    private void initTransition() {
        if (isTransition && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            postponeEnterTransition();
            ViewCompat.setTransitionName(normalGSYVideoPlayer, IMG_TRANSITION);
            addTransitionListener();
            startPostponedEnterTransition();
        } else {
            normalGSYVideoPlayer.startPlayLogic();
        }
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private boolean addTransitionListener() {
        transition = getWindow().getSharedElementEnterTransition();
        if (transition != null) {
            transition.addListener(new OnTransitionListener(){
                @Override
                public void onTransitionEnd(Transition transition) {
                    super.onTransitionEnd(transition);
                    normalGSYVideoPlayer.startPlayLogic();
                    transition.removeListener(this);
                }
            });
            return true;
        }
        return false;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(orientationUtils.getScreenType()== ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE){
            normalGSYVideoPlayer.getFullscreenButton().performClick();
            return;
        }
        //释放所有
        normalGSYVideoPlayer.setStandardVideoAllCallBack(null);
        NormalGSYVideoPlayer.releaseAllVideos();

        if(isTransition&& Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
            super.onBackPressed();
        }else{
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                    overridePendingTransition(R.anim.put_up_in,R.anim.put_up_out);
                }
            },500);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //恢复暂停状态
        normalGSYVideoPlayer.onVideoResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //
        normalGSYVideoPlayer.onVideoPause();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(orientationUtils!=null){
            orientationUtils.releaseListener();
        }
    }
}
