package com.example.huyahui.rxdemo;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.EasyPermissions;
import view.CustomLinearLayout;
import view.CustomTextView;

public class ViewsActivity extends AppCompatActivity {

    @BindView(R.id.btn)
    Button btn;
    @BindView(R.id.view)
    CustomTextView view;
    @BindView(R.id.cll)
    CustomLinearLayout cll;

    private static final String TAG = "ViewsActivity";
    public final int REQUEST_CODE=101;
    public final int REQUEST_CODE_SMS=102;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_views);
        ButterKnife.bind(this);

        cll.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG, "onTouch: 这是ViewGroup----onTouch");
                Toast.makeText(ViewsActivity.this, "这是ViewGroup----onTouch", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG, "onTouch: 这是View----onTouch");
                Toast.makeText(ViewsActivity.this, "这是View----onTouch", Toast.LENGTH_SHORT).show();
                return true;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
       // getPermission();
    }

    /**
     * 检查权限
     */
    public void getPermission(){
        String[] perms = {Manifest.permission.CAMERA, Manifest.permission.CHANGE_WIFI_STATE};
        if(EasyPermissions.hasPermissions(this,"android.permission.READ_CONTACTS")){
            Toast.makeText(this, "有读取联系人权限", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "没有读取联系人权限", Toast.LENGTH_SHORT).show();
            EasyPermissions.requestPermissions(this,"需要权限",REQUEST_CODE,perms);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


    /**
     * 点击textView，事件被CustomLinearlayout拦截，最终被textview消费
     * 点击其他地方,事件被CustomLinearlayout拦截，被CustomLinearlayout消费
     *
     * @param view
     */

    @OnClick({R.id.btn, R.id.view})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn:
                Log.d(TAG, "onViewClicked: button---这是Activity");
                getLongth();
                break;
            case R.id.view:
                Log.d(TAG, "onViewClicked: TextView---这是Activity");
                break;
            default:
                break;
        }
    }



    public void getLongth(){
        String s="hello world i am from China";
        String [] arry=s.split(" ");
        if(arry!=null&&arry.length>0){
            Log.d(TAG, "getLongth: 最后一个单词长度:"+arry[arry.length-1].length());
        }
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Log.d(TAG, "dispatchTouchEvent: --事件分发--这是Activity");
        switch (ev.getAction()){
            case MotionEvent.ACTION_DOWN:

                break;
            case MotionEvent.ACTION_MOVE:

                break;
            case MotionEvent.ACTION_UP:

                break;
            default:

                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d(TAG, "onTouchEvent: --事件消费--这是Activity");
        return super.onTouchEvent(event);
    }

}
