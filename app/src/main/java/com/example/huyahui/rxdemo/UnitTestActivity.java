package com.example.huyahui.rxdemo;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SpanUtils;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author: huyahui
 * @date: 2018/3/14
 */
public class UnitTestActivity extends AppCompatActivity {


    @BindView(R.id.tv_text)
    TextView tvText;
    @BindView(R.id.edt_name)
    EditText edtName;
    @BindView(R.id.btn)
    Button btn;
    @BindView(R.id.tb_test)
    ToggleButton tbTest;
    @BindView(R.id.unit_linear)
    LinearLayout unitLinear;

    /**
     * 随机设置背景图
     */
    Random random=new Random();
    Intent[] intents=new Intent[3];
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unit_test);
        ButterKnife.bind(this);
        initView();
    }


    public void initView() {
        tbTest.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                   /* tvText.setText(new SpanUtils()
                            .appendLine("胡亚辉")
                            .appendLine("好人")
                            .appendImage(AppUtils.getAppIcon(),SpanUtils.ALIGN_CENTER).create());*/
                    tvText.setText(new SpanUtils()
                            .appendLine("胡亚辉")
                            .appendLine(AppUtils.getAppName())
                            .create());
                } else {
                    tvText.setText("FALSE");
                }
            }
        });
        intent=new Intent(UnitTestActivity.this,QmuiActivity.class);
        intents[0]=intent;
        intents[1]=new Intent(UnitTestActivity.this,AlgorithmActivity.class);
        intents[2]=new Intent(UnitTestActivity.this,StarActivity.class);

    }

    @Override
    protected void onResume() {
        super.onResume();
        //根布局的背景颜色随机组合
        unitLinear.setBackgroundColor(Color.rgb(random.nextInt(256), random.nextInt(256), random.nextInt(256)));
    }

    @OnClick({R.id.tv_text, R.id.edt_name, R.id.btn, R.id.tb_test})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_text:
                break;
            case R.id.edt_name:
                break;
            case R.id.btn:
                startActivities(intents);
                break;
            case R.id.tb_test:
                break;
            default:
                break;
        }
    }
}
