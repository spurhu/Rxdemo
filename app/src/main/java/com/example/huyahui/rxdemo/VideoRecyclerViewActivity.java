package com.example.huyahui.rxdemo;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;
import android.util.Log;
import android.view.Window;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;
import com.shuyu.gsyvideoplayer.video.base.GSYVideoPlayer;
import java.util.ArrayList;
import java.util.List;
import adapter.RecyclerBaseAdapter;
import adapter.RecyclerNormalAdapter;
import entity.VideoModel;
import video.bean.RecyclerItemNormalHolder;

public class VideoRecyclerViewActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    RecyclerBaseAdapter recyclerBaseAdapter;
    List<VideoModel> list = new ArrayList<>();

    boolean full = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            getWindow().setEnterTransition(new Explode());
            getWindow().setEnterTransition(new Explode());
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_recycler_view);
        recyclerView=(RecyclerView)findViewById(R.id.recyclerview);

        resolvaData();

        final RecyclerNormalAdapter recyclerNormalAdapter=new RecyclerNormalAdapter(this,list) ;
        linearLayoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(recyclerNormalAdapter);

        //将被通知任何滚动状态或位置的变化。
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            int firstItem,lastItem;
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.d("VideoRecyclerViewActivi", "---滚动状态---"+newState);
                //newStatue 0:不滚动  1：开始滚动  2：连续滚动
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //dx用来判断横向滑动方向，dy用来判断纵向滑动方向
                if(dy>0){
                    Log.d("VideoRecyclerViewActivi", "向上滚动");
                }else{
                    Log.d("VideoRecyclerViewActivi", "向下滚动");
                }

                firstItem=linearLayoutManager.findFirstVisibleItemPosition();
                lastItem=linearLayoutManager.findLastVisibleItemPosition();
                //获取播放位置,如果大于0说明有播放
                if(GSYVideoManager.instance().getPlayPosition()>=0){
                    //当前播放位置
                    int position=GSYVideoManager.instance().getPlayPosition();
                    //对应的播放列表
                    if(GSYVideoManager.instance().getPlayTag().equals(RecyclerItemNormalHolder.TAG)&&
                            (position<firstItem||position>lastItem)){
                        //position<firstItem||position>lastItem 表示滑出去了 不在当前屏幕可视位置
                        //是否全屏
                        if(!full){
                            GSYVideoPlayer.releaseAllVideos();
                            recyclerNormalAdapter.notifyDataSetChanged();
                        }
                        //如果不是全屏的情况下，画出屏幕范围，adapter刷新，当前播放的视频停止
                    }

                }
            }
        });
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //如果旋转了就全屏
        if(newConfig.orientation!= ActivityInfo.SCREEN_ORIENTATION_USER){
            full=false;
        }else{
            full=true;
        }
       /*
        可以在这里声明activity可以处理的任何配置改变，当这些配置改变时不会重新启动activity，而会调用activity的
        onConfigurationChanged(Resources.Configuration)方法。如果改变的配置中包含了你所无法处理的配置（在android:configChanges并未声明），
        你的activity仍然要被重新启动，而onConfigurationChanged(Resources.Configuration)将不会被调用。*/
    }


    @Override
    public void onBackPressed() {
        //如果是全屏的情况下,点击返回不执行操作(退出全屏)
        if (StandardGSYVideoPlayer.backFromWindowFull(this)) {
            return;//跳出整个函数体，函数体后面的部分不再执行
        }
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        GSYVideoManager.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        GSYVideoManager.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //页面销毁，释放所有video
        GSYVideoPlayer.releaseAllVideos();
    }

    /**
     * 初始化集合数据
     */
    private void resolvaData() {
        for (int i = 0; i < 19; i++) {
            VideoModel videoModel = new VideoModel();
            list.add(videoModel);
        }
        if (recyclerBaseAdapter != null) {
            //如果数据改变,刷新适配器
            recyclerBaseAdapter.notifyDataSetChanged();
        }
    }

}
