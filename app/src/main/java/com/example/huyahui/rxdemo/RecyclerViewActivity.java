package com.example.huyahui.rxdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.util.LogTime;

import java.util.ArrayList;
import java.util.List;

import adapter.GuideAdapter;

/**
 * https://www.jianshu.com/p/ce347cf991db
 *
 */
public class RecyclerViewActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private TextView textView;

    private GuideAdapter guideAdapter;
    private List<String> list = new ArrayList<>();

    private static final String TAG = "RecyclerViewActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);
        initView();
    }


    public  void initView(){
       recyclerView=(RecyclerView)findViewById(R.id.recycler);
       textView=(TextView)findViewById(R.id.tv);
       for(int i=0;i<30;i++){
           list.add("明朝那些事儿"+i);
       }
       final LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
       recyclerView.setLayoutManager(linearLayoutManager);
       guideAdapter=new GuideAdapter(list,this);
       recyclerView.setAdapter(guideAdapter);
       recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
           @Override //滚动状态变化是回调
           public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
               super.onScrollStateChanged(recyclerView, newState);
               // 0:静止没有滚动, 1:正在被外部拖拽,  2:自动滚动
               Log.d(TAG, "onScrollStateChanged:  newState"+newState);

           }

           @Override // 滚动是回调
           public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
               super.onScrolled(recyclerView, dx, dy);
               Log.d(TAG, "onScrolled:  dx "+dx);
               Log.d(TAG, "onScrolled:  dy "+dy);
               textView.setAlpha(dy);
//               if(dy>0){
//                   Toast.makeText(RecyclerViewActivity.this, "上滑", Toast.LENGTH_SHORT).show();
//               }else{
//                   Toast.makeText(RecyclerViewActivity.this, "下滑", Toast.LENGTH_SHORT).show();
//               }

               if(recyclerView.canScrollHorizontally(-1)){
                   Toast.makeText(RecyclerViewActivity.this, "屏幕上滑", Toast.LENGTH_SHORT).show();
               }


               if (dy > 0) //向下滚动
               {
                   int visibleItemCount = linearLayoutManager.getChildCount();
                   int totalItemCount = linearLayoutManager.getItemCount();
                   int pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                   if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                       Toast.makeText(RecyclerViewActivity.this, "滑到底部了", Toast.LENGTH_SHORT).show();
                   }
               }


           }
       });


       recyclerView.canScrollHorizontally(-1); // 竖直方向是否还能向上，向下滑动
       recyclerView.canScrollVertically(-1); // 水平方向的滑动

       // 可能出现空指针 被弃用
      /* recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
           @Override
           public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
               super.onScrollStateChanged(recyclerView, newState);
           }

           @Override
           public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
               super.onScrolled(recyclerView, dx, dy);
               Log.d(TAG, "onScrolled:  ----dx "+dx);
               Log.d(TAG, "onScrolled:  ----dy "+dy);
           }
       });*/


       guideAdapter.setOnItemListener(new GuideAdapter.OnItemClickListener() {
           @Override
           public void OnItemClick(int position) {
               Log.d(TAG, "onScrolled:  ----点击事件 ");
               ToastUtils.showLong("点击事件");
           }
       });

  /*     recyclerView.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View view, MotionEvent motionEvent) {
               Log.d(TAG, "onScrolled:  ----motionEvent ");
               return true;
           }

       });*/

    }

}
