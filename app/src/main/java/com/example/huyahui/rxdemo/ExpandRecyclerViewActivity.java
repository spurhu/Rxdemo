package com.example.huyahui.rxdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import java.util.ArrayList;
import Expand.ExpandLayoutHelper;
import Expand.ItemClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import entity.Item;
import entity.Section;
import util.MyToast;

/**
 * Demo  class
 *
 * @author: huyahui
 * @date: 2018/1/20
 */
public class ExpandRecyclerViewActivity extends AppCompatActivity implements ItemClickListener {


    @BindView(R.id.tv)
    TextView tv;
    @BindView(R.id.expend_recyclerview)
    RecyclerView expendRecyclerview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expand_recycler_view);
        ButterKnife.bind(this);
        initRecyclerview();
    }

    /**
     * 初始化V
     */
    private void initRecyclerview() {
        ExpandLayoutHelper expandLayoutHelper = new ExpandLayoutHelper(this, expendRecyclerview,
                this, 1);
        ArrayList<Item> arrayList = new ArrayList<>();
        arrayList.add(new Item("美的", 0));
        arrayList.add(new Item("海尔", 1));
        arrayList.add(new Item("苏泊尔", 2));
        arrayList.add(new Item("方太", 3));
        expandLayoutHelper.addSection("家用电器", arrayList);
        arrayList = new ArrayList<>();
        arrayList.add(new Item("安踏", 0));
        arrayList.add(new Item("李宁", 1));
        arrayList.add(new Item("美特斯邦威", 2));
        arrayList.add(new Item("匹克", 3));
        expandLayoutHelper.addSection("运动品牌", arrayList);

        expandLayoutHelper.notifyDataSetChanged();

        expandLayoutHelper.addItem("运动品牌", new Item("361", 4));
        expandLayoutHelper.notifyDataSetChanged();
    }


    @Override
    public void itemClicked(Item item) {
        MyToast.showMessage(this, "Item: " + item.getName() + " clicked");
    }

    @Override
    public void itemClicked(Section section) {
        MyToast.showMessage(this, "Section: " + section.getName() + " clicked");
    }
}
