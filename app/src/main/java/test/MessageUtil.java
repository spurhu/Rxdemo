package test;

/**
 * @author: huyahui
 * @date: 2018/3/15
 */

public class MessageUtil {


    private String msg;

    public MessageUtil(String msg) {
        this.msg = msg;
    }


    public String printMsg(){
        System.out.print(msg);
        return msg;
    }


    public String salutationMessage(){

        msg="HI"+msg;
        System.out.print(msg);
        return msg;
    }


    /**
     * 冒泡排序
     * @param list
     */
    public int[] bubbleSort(int[] list) {
        int t;
        for (int i = 0; i < list.length; i++) {
            for (int j = 0; j < list.length - 1 - i; j++) {
                if(list[j]>list[j + 1]){
                    t = list[j];
                    list[j] = list[j + 1];
                    list[j + 1] = t;
                }
            }
        }
        return list;
    }




}
