package test;

import view.ObservableScrollView;

/**
 * @author: huyahui
 * @date: 2018/4/26
 */

public interface ScrollViewListener {

    void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy);
}
