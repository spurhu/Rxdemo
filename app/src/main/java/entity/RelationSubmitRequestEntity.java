package entity;

/**
 * Title:RelationSubmitRequestEntity
 * Package:com.tomcat360.m.requestEntity
 * Description:TODO
 * Author: wwh@tomcat360.com
 * Date: 17/4/21
 * Version: V1.0.0
 * 版本号修改日期修改人修改内容
 */

public class RelationSubmitRequestEntity {
	private String name;
	private String mobile;
	private String relate;
	private String nickname;
	private String id;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getRelate() {
		return relate;
	}

	public void setRelate(String relate) {
		this.relate = relate;
	}


	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
