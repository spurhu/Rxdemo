package entity;

import rx.schedulers.Schedulers;

/**
 * @author: huyahui
 * @date: 2018/1/30
 */

public class Person {

    private String name;
    private String time;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Person(String name, String time) {
        this.name = name;
        this.time = time;
    }
}
