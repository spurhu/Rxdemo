package entity;

/**
 * @author: huyahui
 * @date: 2018/5/3
 */

public class Section {

    private final String name;

    public boolean isExpanded;

    public Section(String name) {
        this.name = name;
        isExpanded = true;
    }

    public String getName() {
        return name;
    }

}
