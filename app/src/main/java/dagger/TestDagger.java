package dagger;

import javax.inject.Inject;

/**
 * @author: huyahui
 * @date: 2018/2/1
 */

public class TestDagger {


    @Inject
    Distiller distiller;

   /* public TestDagger() {
        DaggerBrandyComponent.create().inject(this);    //在代码中我们并没有对 Distiller 对象进行 new 操作来实例化
    }*/


    @Override
    public String toString(){
        return distiller.toString();
    }


}
