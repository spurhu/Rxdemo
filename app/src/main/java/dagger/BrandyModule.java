package dagger;

/**
 * @author: huyahui
 * @date: 2018/2/1
 */
@Module
public class BrandyModule {

   /* @Module 可以理解为一个生产实例的工厂，他掌握各个需要注入的类的实例化方法，当 Dagger 需要为某个类注入实例时，
    会到 @Module 注解的类中，查找这个类的实例化方法。当然这一过程是需要通过使用
    @Provides 注解的有返回值的方法，来告知 Dagger 的。*/

    @Provides
    public Grape providerGrape(){
        return new Grape("解百纳");
    }

}
