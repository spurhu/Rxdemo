package dagger;

import rx.schedulers.Schedulers;

/**
 * @author: huyahui
 * @date: 2018/2/1
 */

public class Grape {
    private String name;
    private String color;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Grape() {
    }

    public Grape(String name) {
        this.name = name;
    }

    public Grape(String name, String color) {
        this.name = name;
        this.color = color;
    }
}
