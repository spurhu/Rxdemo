package dagger;

import com.example.huyahui.rxdemo.MessageActivity;

/**
 * @author: huyahui
 * @date: 2018/2/1
 */
@Component(modules = BrandyModule.class)
public interface BrandyComponent {
/*    @Component 用来注解一个接口，在编译的时候会生成 Dagger+文件名 的新Java文件。  " DaggerBrandyComponent"
     Component可以理解为注射器，它是连接被注入的类与需要被注入的类之间的桥梁。*/

//    void inject(MessageActivity messageActivity);


    void inject(TestDagger testDagger);



}
