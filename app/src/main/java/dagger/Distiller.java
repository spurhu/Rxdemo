package dagger;

import javax.inject.Inject;

/**
 * @author: huyahui
 * @date: 2018/2/1
 */

public class Distiller {

/*    @Inject 有两个功能：
            1、注解类的构造方法，其作用可以理解为通过注解将这个类的实例化方法告诉 Dagger；
            2、在需要使用该实例的地方注解，其作用是告诉调用者，这个被注解的实例由 Dagger 来负责实例化；
            */


    @Inject
    public Distiller(){

    }

    @Override
    public String toString(){
        return "蒸馏器";
    }

}
