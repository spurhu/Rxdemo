package adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.example.huyahui.rxdemo.R;

import java.util.List;

/**
 * ******************************
 *
 * @author nihao
 * @descriptions Rxdemo
 * @email hujourney365@gmail.com
 * @date 2018/9/13
 * ******************************
 */
public class ImageAdapter  extends  RecyclerView.Adapter<ImageAdapter.ImageVideHolder>{

   private Context context;
   private List<String> list;

    public ImageAdapter(Context context, List<String> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ImageVideHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_image,null);
        return new ImageVideHolder(view);
    }

    @Override
    public void onBindViewHolder(ImageVideHolder holder, int position) {
         //过渡动画
        Glide.with(context).load(Uri.parse(list.get(position))).transition(new DrawableTransitionOptions().crossFade(300)).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return list==null?0:list.size();
    }

    public class ImageVideHolder extends RecyclerView.ViewHolder{
        private ImageView imageView;
        public ImageVideHolder(View itemView) {
            super(itemView);
            imageView=(ImageView)itemView.findViewById(R.id.image);
        }
    }


}
