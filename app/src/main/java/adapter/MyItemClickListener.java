package adapter;

import android.view.View;

/**
 * @author: huyahui
 * @date: 2018/4/23
 */

public interface MyItemClickListener {

    public void OnItemClick(View view,int position);
}
