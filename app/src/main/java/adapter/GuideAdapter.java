package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.huyahui.rxdemo.R;

import java.util.List;

/**
 * Created by huyahui on 2017/9/6.
 */

public class GuideAdapter extends RecyclerView.Adapter<GuideAdapter.GuideHolder> {

    private List<String> list;
    private Context context;


    public GuideAdapter(List<String> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public interface  OnItemClickListener{
        void OnItemClick(int position);
    }

    private OnItemClickListener onItemListener;

    public void setOnItemListener(OnItemClickListener onItemListener) {
        this.onItemListener = onItemListener;
    }

    @Override
    public GuideHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_layout,parent,false);
        return new GuideHolder(view);
    }

    @Override
    public void onBindViewHolder(GuideHolder holder, final int position) {
        holder.textView.setText(list.get(position).toString());
        if(onItemListener!=null){
              holder.itemView.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
               onItemListener.OnItemClick(position);
              }
             });
        }

    }

    @Override
    public int getItemCount() {
        return list==null?0:list.size();
    }

    public class GuideHolder extends RecyclerView.ViewHolder{

        private TextView textView;

        public GuideHolder(View itemView) {
            super(itemView);
            textView=(TextView)itemView.findViewById(R.id.tv_text);
        }
    }


}
