package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.huyahui.rxdemo.R;
import java.util.List;
import entity.VideoModel;
import util.SmallVideoHelper;
import video.bean.RecyclerItemViewHolder;

/**
 * @author: huyahui
 * @date: 2018/2/6
 */

public class RecyclerBaseAdapter extends RecyclerView.Adapter {

    private final static String Tag="RecyclerBaseAdapter";
    private List<VideoModel> videoModels;
    private Context context=null;
    //列表模式的小屏和全屏工具类
    private SmallVideoHelper  smallVideoHelper;
    private SmallVideoHelper.GSYSmallVideoHelperBuilder gsySmallVideoHelperBuilder;

    public RecyclerBaseAdapter(List<VideoModel> videoModels, Context context) {
        this.videoModels = videoModels;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.recycle_item,parent,false);
        RecyclerView.ViewHolder holder=new RecyclerItemViewHolder(context,view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        RecyclerItemViewHolder recyclerItemViewHolder=(RecyclerItemViewHolder)holder;
        recyclerItemViewHolder.setVideoHelper(smallVideoHelper,gsySmallVideoHelperBuilder);
        recyclerItemViewHolder.setRecyclerBaseAdapter(this);
        //绑定数据
        recyclerItemViewHolder.onBind(position,videoModels.get(position));
    }

    @Override
    public int getItemCount() {
        return videoModels.size();
    }


    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    public void setSmallVideoHelper(SmallVideoHelper smallVideoHelper,SmallVideoHelper.GSYSmallVideoHelperBuilder gsySmallVideoHelperBuilder){
        this.smallVideoHelper=smallVideoHelper;
        this.gsySmallVideoHelperBuilder=gsySmallVideoHelperBuilder;
    }



}
