package adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.huyahui.rxdemo.R;
import java.util.List;

import butterknife.OnItemClick;
import entity.Person;

/**
 * @author: huyahui
 * @date: 2018/2/2
 */

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.CardHolder>{

    private List<Person> list;
    private Context context;

    public CardAdapter(List<Person> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public interface OnItemClickListener{
        void OnItemClick(int position);
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public CardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.recycle_item,parent,false);
        return new CardHolder(view);
    }


    @Override
    public void onBindViewHolder(CardHolder holder, int position) {
        RequestOptions options=new RequestOptions();
        options.placeholder(R.mipmap.sun);
        options.diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(context).load(Uri.parse(list.get(position).getName())).into(holder.imageView);
        holder.tv.setText(list.get(position).getTime());

        holder.cardView.setRadius(10);//设置图片圆角的半径大小
        holder.cardView.setCardElevation(8);//设置阴影部分大小
    }


    @Override
    public int getItemCount() {
        return list==null?0:list.size();
    }


    public class CardHolder extends RecyclerView.ViewHolder{
        private CardView cardView;
        private ImageView imageView;
        private TextView tv;

        public CardHolder(View itemView) {
            super(itemView);
            imageView=(ImageView)itemView.findViewById(R.id.imgview);
            tv=(TextView)itemView.findViewById(R.id.tvinfo);
            cardView=(CardView)itemView.findViewById(R.id.cardview);
        }
    }




}
