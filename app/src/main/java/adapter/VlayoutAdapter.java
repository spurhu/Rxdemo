package adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.alibaba.android.vlayout.DelegateAdapter;
import com.alibaba.android.vlayout.LayoutHelper;
import com.alibaba.android.vlayout.VirtualLayoutManager;
import com.example.huyahui.rxdemo.R;
import java.util.List;

/**
 * @author: huyahui
 * @date: 2018/4/23
 */

public class VlayoutAdapter extends DelegateAdapter.Adapter<VlayoutAdapter.VlayoutHolder> {

    private List<String> list;
    private Context context;
    private LayoutHelper layoutHelper;
    private int count=0;
    private VirtualLayoutManager.LayoutParams params;

    private MyItemClickListener myItemClickListener;
    // 用于设置Item点击事件


    public VlayoutAdapter(List<String> list, Context context, LayoutHelper layoutHelper, int count, VirtualLayoutManager.LayoutParams params) {
        this.list = list;
        this.context = context;
        this.layoutHelper = layoutHelper;
        this.count = count;
        this.params = params;
    }


    public VlayoutAdapter(List<String> list, Context context, LayoutHelper layoutHelper, int count) {
        this.list = list;
        this.context = context;
        this.layoutHelper = layoutHelper;
        this.count = count;
    }

    /**
     * 传给adapter指定的layoutHelper类来设置item的布局样式
     *
     * @param holder
     * @param position
     * @param offsetTotal
     */
    @Override
    protected void onBindViewHolderWithOffset(VlayoutHolder holder, int position, int offsetTotal) {
        super.onBindViewHolderWithOffset(holder, position, offsetTotal);
    }

    /***
     * 跟原有onBindViewHolder类似，只是多了一个参数offsetTotal，作用是指当前item在所有adapter item总和个数的位置。
     * @return
     */
    @Override
    public LayoutHelper onCreateLayoutHelper() {
        return layoutHelper;
    }



    @Override
    public VlayoutHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.vlayout_item,parent,false);
        return new VlayoutHolder(view);
    }

    @Override
    public void onBindViewHolder(VlayoutHolder holder, final int position) {
        holder.tv_name.setText(list.get(position));
        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myItemClickListener.OnItemClick(v,position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }


    /**
     * 刷新页面，头部增加新数据
     * @param listitem
     */
    public void AddHeadItem(List<String> listitem){
        list.addAll(0,listitem);
        notifyDataSetChanged();
    }

    /**
     * 刷新页面，底部增加新数据
     * @param listitem
     */
    public void AddFooterItem(List<String> listitem){
        list.addAll(listitem);
        notifyDataSetChanged();
    }




    /**
     * 设置Item的点击事件
     * 绑定MainActivity传进来的点击监听器
     *
     * @param myItemClickListener
     */
    public void setMyItemClickListener(MyItemClickListener myItemClickListener) {
        this.myItemClickListener = myItemClickListener;
    }

    public class VlayoutHolder extends RecyclerView.ViewHolder{
        private TextView tv_name;
        public VlayoutHolder(View itemView) {
            super(itemView);
            tv_name=(TextView)itemView.findViewById(R.id.textView);
        }
    }



}
