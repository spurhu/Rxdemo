package util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.blankj.utilcode.util.Utils;

import static com.blankj.utilcode.util.ActivityUtils.startActivity;

/**
 * @author: huyahui
 * @date: 2018/4/23
 */

public class ActivityUtils {

    @SuppressLint("StaticFieldLeak")
    private static MyApplication sApplication;


    public void goActivity(Context context, Activity activity){
        Intent intent=new Intent(context,activity.getClass());
        startActivity(intent);
    }

    public static void init(@NonNull final Context context) {
        ActivityUtils.sApplication = (MyApplication) context.getApplicationContext();
    }


    public static MyApplication getApp() {
        if (sApplication != null){
            return sApplication;
        }
        throw new NullPointerException("u should init first");
    }



}
