package util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
/**
 * @author xst
 *         保留几位小数点
 */
public final class DateUtils {
    public static final long MILLIS_OF_SECOND = 1000L;
    public static final long MILLIS_OF_MINUTE = 60 * MILLIS_OF_SECOND;
    public static final long MILLIS_OF_HOUR = 60 * MILLIS_OF_MINUTE;
    public static final long MILLIS_OF_DAY = 24 * MILLIS_OF_HOUR;
    public static final long MILLIS_OF_WEEK = 7 * MILLIS_OF_DAY;
    public static final long ROUGH_MILLIS_OF_MONTH = 30 * MILLIS_OF_DAY;

    /**
     * @param value
     * @param format like 0.00
     * @return
     */
    public static String getNumberString(final double value, final String format) {
        final DecimalFormat df = new DecimalFormat(format);
        return df.format(value);
    }

    /**
     * @param value
     * @return 自动截取合适的小数点
     */
    public static String getAutoNumberString(final double value) {
        final String strValue = getNumberString(value, "#.##");
        return strValue;
    }

    public static String getNumberStringInt(final double value) {
        return getNumberString(value, "0");
    }

    public static String getNumberOne(final double value) {
        return getNumberString(value, "0.0");
    }

    public static String getNumberTwo(final double value) {
//        BigDecimal bd = new BigDecimal(value);
//        BigDecimal setScale = bd.setScale(2, bd.ROUND_DOWN);
//        return setScale.toPlainString();
          return  calculateProfit(value);
    }

    public static String getNumberSeven(final double value) {
        return getNumberString(value, "0.0000000");
    }

    /**
     * 保留double类型小数后两位，不四舍五入，直接取小数后两位 比如：10.1269 返回：10.12
     *
     * @param doubleValue
     * @return
     */
    public static String calculateProfit(double doubleValue) {
        // 保留4位小数
        DecimalFormat df = new DecimalFormat("#,###.0000");
        String result = df.format(doubleValue);

        // 截取第一位
        String index = result.substring(0, 1);

        if (".".equals(index)) {
            result = "0" + result;
        }
        // 获取小数 . 号第一次出现的位置
        int inde = firstIndexOf(result, ".");
        // 字符串截断
        String result2 = result.substring(0, inde + 3);
        return result2;

    }

    /**
     * 查找字符串pattern在str中第一次出现的位置
     *
     * @param str
     * @param pattern
     * @return
     */
    public static int firstIndexOf(String str, String pattern) {
        for (int i = 0; i < (str.length() - pattern.length()); i++) {
            int j = 0;
            while (j < pattern.length()) {
                if (str.charAt(i + j) != pattern.charAt(j))
                    break;
                j++;
            }
            if (j == pattern.length())
                return i;
        }
        return -1;
    }



    public static double getPercentageNumber(double value) {
        return value * 100;
    }


    public static String getDate(int day) {
        Date date = new Date();
        SimpleDateFormat s = new SimpleDateFormat("yyyy'年'MM'月'dd'日'");
        System.out.println(s.format(date));
//        String str1 = s.format(date);//当前的时间
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, day);//计算30天后的时间
        String str2 = s.format(c.getTime());
//		System.out.println("day天后的时间是："+str2);
        return str2;
    }

    public static String getDate2(int day) {
        Date date = new Date();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println(s.format(date));
//        String str1 = s.format(date);//当前的时间
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, day);//计算day天后的时间
        String str2 = s.format(c.getTime());
//		System.out.println("day天后的时间是："+str2);
        return str2;
    }

    public static String getDate4(int day) {
        Date date = new Date();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(s.format(date));
//        String str1 = s.format(date);//当前的时间
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, day);//计算day天后的时间
        String str2 = s.format(c.getTime());
//		System.out.println("day天后的时间是："+str2);
        return str2;
    }

    public static String getDate3(long time, int day) {
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
//        System.out.println(s.format(time));
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(time);
        c.add(Calendar.DATE, day);//计算day天后的时间
        String str2 = s.format(c.getTime());
        return str2;
    }

    public static String format(long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String d = format.format(time);
        return d;
    }
    public static String format2(long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String d = format.format(time);
        return d;
    }

    public static String getCurrentTime(){
        Date date=new Date();
        DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time=format.format(date);
        return time;
    }

    /**
     * 获取时间的初略描述
     */
   /* public static String getRoughTimeText(Long millisTime) {
        long millis = System.currentTimeMillis() - NumberUtils.valueOf(millisTime);
        if (millis < MILLIS_OF_MINUTE) {
            return "刚刚";
        }
        StringBuilder sb = new StringBuilder();
        if (millis < MILLIS_OF_HOUR) {
            sb.append((int) (millis / MILLIS_OF_MINUTE)).append("分钟");
        } else if (millis < MILLIS_OF_DAY) {
            sb.append((int) (millis / MILLIS_OF_HOUR)).append("小时");
        } else if (millis < MILLIS_OF_WEEK) {
            sb.append((int) (millis / MILLIS_OF_DAY)).append("天");
        } else if (millis < ROUGH_MILLIS_OF_MONTH) {
            sb.append((int) (millis / MILLIS_OF_WEEK)).append("周");
        } else {
            sb.append((int) (millis / ROUGH_MILLIS_OF_MONTH)).append("月");
        }
        sb.append("前");
        return sb.toString();
    }*/

}
