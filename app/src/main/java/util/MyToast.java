package util;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

import com.blankj.utilcode.util.ToastUtils;

/**
 * @author: huyahui
 * @date: 2018/4/24
 */

public class MyToast {


    public static void showMessage(Context context ,String str){
        Toast toast=Toast.makeText(context,str,Toast.LENGTH_SHORT);
//        toast.setGravity(Gravity.BOTTOM,0,0);
        toast.setGravity(Gravity.CENTER,0,0);
//        toast.setMargin(20.0f,20.0f);
        toast.show();
    }


}
