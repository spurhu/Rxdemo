package util;

/**
 * Demo  消息事件类
 *
 * @author: huyahui
 * @date: 2018/1/18
 */

public class MessageEvent {

     private String message;

    public MessageEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }


    public void setMessage(String message) {
        this.message = message;
    }
}
