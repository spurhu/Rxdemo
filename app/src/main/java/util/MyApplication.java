package util;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import timber.log.Timber;

/**
 * Demo  class
 *
 * @author: huyahui
 * @date: 2018/1/17
 */

public class MyApplication extends Application {

    private static RefWatcher mRefWatcher;

    @Override
    public void onCreate() {
        super.onCreate();
        initLeak();
        //在这里先使用Timber.plant注册一个Tree，然后调用静态的.d .v 去使用

        Timber.plant(new Timber.DebugTree());
    }

    /**
     * 初始化Leak
     */
    public void initLeak() {
        //在初始化之前先是调用LeakCanary的静态方法isInAnalyserProcess()做过滤，
        // 如果该方法返回true就直接返回否则就执行LeakCanary的install()方法进行初始化工作
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        LeakCanary.install(this);//开启内存监控
    }








}
