package util;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;


/**
 * ******************************
 *
 * @author nihao
 * @descriptions Rxdemo
 * @email hujourney365@gmail.com
 * @date 2018/10/13
 * ******************************
 */

@GlideModule
public class MyAppGlideModule extends AppGlideModule {

    @Override
    public boolean isManifestParsingEnabled() {
        return false;
    }


}
