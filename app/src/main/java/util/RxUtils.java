package util;

import android.content.Context;
import android.content.Intent;
import android.nfc.Tag;
import android.util.Log;

import com.example.huyahui.rxdemo.MainActivity;
import com.example.huyahui.rxdemo.R;

import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Demo  class
 *
 * @author: huyahui
 * @date: 2018/1/23
 */

public class RxUtils {

    /**
     * rx操作符 create 使用Create操作符从头开始创建一个Observable
     */
    public static void create(){
        Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {

                for(int i=0;i<5;i++){
                    if(subscriber.isUnsubscribed()){//判断是否订阅
                        subscriber.onCompleted();
                    }
                }
            }
        }).subscribe(new Subscriber<String>() {
            @Override
            public void onCompleted() {
                Log.d("MainActivity", "create---------onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                Log.d("MainActivity", "create---------onError");
            }

            @Override
            public void onNext(String s) {
                Log.d("MainActivity", "create---------onNext");
            }
        });

    }

    /**
     * rx操作符from 顺序输出数组内容  将其他种类的对象和数据类型转换为Observable
     */
    public static void from() {
        String[] names = {"A", "B", "C", "D", "E"};
        Observable.from(names).subscribe(new Action1<String>() {
            @Override
            public void call(String s) {
                Log.d("MainActivity", s);
            }
        });
//        tvText.setText(sb);
    }

    /**
     * rx操作符 just相当于create创建一个字符串
     */
    public static void just(){
        Observable.just("胡亚辉").subscribe(new Subscriber<String>() {
            @Override
            public void onCompleted() {
                Log.d("MainActivity", "just--------onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                Log.d("MainActivity", "just--------onError");

            }

            @Override
            public void onNext(String s) {
                Log.d("MainActivity", "just--------onNext");

            }
        });
    }

    /**
     * rx操作符 range 结果：输出五次log
     */
    public static void range(){
        Observable.range(1,5).subscribe(new Subscriber<Integer>() {
            @Override
            public void onCompleted() {
                Log.d("MainActivity", "range-----onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                Log.d("MainActivity", "range-----onError");
            }

            @Override
            public void onNext(Integer integer) {
                Log.d("MainActivity", "range-----onNext");
            }
        });
    }

    /**
     * rx操作符 interval
     */
    public static void interval(){
        //参数：初始延迟，间隔时间，时间单位
      /*  Observable.interval(1,1, TimeUnit.SECONDS).subscribe(new Subscriber<Long>() {
            @Override
            public void onCompleted() {
                Log.d("RxUtils", "interval-----onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                Log.d("RxUtils", "interval-----onError");
            }

            @Override
            public void onNext(Long aLong) {
                Log.d("RxUtils", "interval-----onNext");
            }
        });*/

      //1.创建观察者
      Subscriber subscriber=new Subscriber() {
          @Override
          public void onCompleted() {
              Log.d("RxUtils", "interval-----onCompleted------");
          }

          @Override
          public void onError(Throwable e) {
              Log.d("RxUtils", "interval-----onError");
          }

          @Override
          public void onNext(Object o) {
              Log.d("RxUtils", "interval-----onNext---------");
          }
      };

      //2.可观察者 OnSubscribe返回一个Observable类型变量
      Observable observable=Observable.interval(1,1,TimeUnit.SECONDS);

        //3.订阅事件
        observable.subscribe(subscriber);

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

//        if (!subscriber.isUnsubscribed()) {
//            subscriber.unsubscribe();
//            Log.d("RxUtils", "取消订阅");
//        }

    }


    /**
     * rx操作符 filter  过滤某些条件
     */
    public static void filter(){
        Observable observable= Observable.just(1,2,3,4,5,6);
        observable.filter(new Func1<Integer,Boolean>() {
            @Override
            public Boolean call(Integer integer) {
                return integer<5;
            }
        }).subscribe(new Subscriber() {
            @Override
            public void onCompleted() {
                Log.d("RxUtils", "filter--------onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                Log.d("RxUtils", "filter--------onError");
            }

            @Override
            public void onNext(Object o) {
                Log.d("RxUtils", "filter--------onNext");
            }
        });
//        01-23 15:46:15.434 5931-5931/? D/RxUtils: filter--------onNext
//        01-23 15:46:15.434 5931-5931/? D/RxUtils: filter--------onNext
//        01-23 15:46:15.434 5931-5931/? D/RxUtils: filter--------onNext
//        01-23 15:46:15.434 5931-5931/? D/RxUtils: filter--------onNext
//        01-23 15:46:15.434 5931-5931/? D/RxUtils: filter--------onCompleted
    }




}
