package util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

/**
 * @author: huyahui
 * @date: 2018/3/2
 */

public class AndroidJavaScript {



    private static final String TAG = "AndroidJavaScript";
    Context c;
    String[] qqpackage = new String[] { "com.tencent.mobileqq",
            "com.tencent.mobileqq.activity.SplashActivity" };
    String[] wxpackage = new String[] { "com.tencent.mm",
            "com.tencent.mm.ui.LauncherUI" };


    private static final String[] PHONES_PROJECTION = new String[] {
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Photo.PHOTO_ID, ContactsContract.CommonDataKinds.Phone.CONTACT_ID };

    private static final int PHONES_DISPLAY_NAME_INDEX = 0;

    private static final int PHONES_NUMBER_INDEX = 1;

    public AndroidJavaScript(Context c) {
        this.c = c;
    }

    @JavascriptInterface
    public void callPhone(final String telphone) {
        Log.e(TAG, "callPhone:"+telphone);

        Intent intent = new Intent(Intent.ACTION_DIAL);
        Uri data = Uri.parse("tel:" + telphone);
        intent.setData(data);
        c.startActivity(intent);
    }


    @JavascriptInterface
    public void showMessage(String message){
        Log.e(TAG, "showMessage:"+message);
        Toast.makeText(c,"---胡亚辉---"+message,Toast.LENGTH_SHORT).show();
    }




    @JavascriptInterface
    public void callQQ(String qq) {
        // 实现调用电话号码

        if (!checkBrowser(qqpackage[0])) {

        } else {
            Intent intent = new Intent();
            ComponentName cmp = new ComponentName(qqpackage[0], qqpackage[1]);
            intent.setAction(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setComponent(cmp);
            c.startActivity(intent);
        }

    }

    @JavascriptInterface
    public void callWeixin(String weixin) {

        if (!checkBrowser(wxpackage[0])) {

        } else {
            Intent intent = new Intent();
            ComponentName cmp = new ComponentName(wxpackage[0], wxpackage[1]);
            intent.setAction(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setComponent(cmp);
            c.startActivity(intent);

        }

    }

    // 获取在webview上获取js生成的html的源码
    @JavascriptInterface
    public void getSource(String htmlstr) {
        // Log.e("html", htmlstr);
        // String path = c.getFilesDir().getAbsolutePath() + "/serve.html"; //
        // data/data目录
    }


    //检测包名的应用是否已经安装在手机
    public boolean checkBrowser(String packageName) {
        if (packageName == null || "".equals(packageName))
            return false;
        try {
            ApplicationInfo info = c.getPackageManager().getApplicationInfo(
                    packageName, PackageManager.GET_UNINSTALLED_PACKAGES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }


   /* *//**
     * 获取手机联系人
     *//*
    @JavascriptInterface
    public String getPhoneContacts() {

        JSONArray json=new JSONArray();

        ContentResolver resolver = c.getContentResolver();

        Cursor phoneCursor = resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,PHONES_PROJECTION, null, null, null);
        JSONObject obj=null;
        if (phoneCursor != null) {
            while (phoneCursor.moveToNext()) {

                String phoneNumber = phoneCursor.getString(PHONES_NUMBER_INDEX);
                if (TextUtils.isEmpty(phoneNumber))
                    continue;
                obj= new JSONObject();
                obj.put("userName",phoneCursor.getString(PHONES_DISPLAY_NAME_INDEX));
                obj.put("mobile",phoneNumber);
                json.add(obj);
            }

            phoneCursor.close();
        }

        return  json.toString();
    }

*/


}
