package Expand;

import entity.Item;
import entity.Section;

/**
 * @author: huyahui
 * @date: 2018/5/3
 */

public interface ItemClickListener {

    void itemClicked(Item item);
    void itemClicked(Section section);

}
