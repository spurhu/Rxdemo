package Expand;

import entity.Section;

/**
 * @author: huyahui
 * @date: 2018/5/3
 */

public interface SectionStateChangeListener {
    void onSectionStateChanged(Section section, boolean isOpen);
}
