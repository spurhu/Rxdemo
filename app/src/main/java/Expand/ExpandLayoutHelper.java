package Expand;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import entity.Item;
import entity.Section;

/**
 * @author: huyahui
 * @date: 2018/5/3
 */

public class ExpandLayoutHelper implements SectionStateChangeListener {

    /**
     * data list
     */
    private LinkedHashMap<Section, ArrayList<Item>> mSectionDataMap = new LinkedHashMap<Section, ArrayList<Item>>();
    private ArrayList<Object> mDataArrayList = new ArrayList<Object>();

    private HashMap<String, Section> mSectionMap = new HashMap<String, Section>();
    private ExpandListAdapter expandListAdapter;

    public ExpandLayoutHelper(Context context, RecyclerView recyclerView,ItemClickListener itemClickListener,
                              int gridSpanCount) {
        GridLayoutManager gridLayoutManager=new GridLayoutManager(context,gridSpanCount);
        recyclerView.setLayoutManager(gridLayoutManager);
        expandListAdapter=new ExpandListAdapter(context,mDataArrayList,itemClickListener,
                this,gridLayoutManager);
        recyclerView.setAdapter(expandListAdapter);

    }


    public void addSection(String section,ArrayList<Item> items){
        Section newSection;
        mSectionMap.put(section,(newSection=new Section(section)));
        mSectionDataMap.put(newSection,items);

    }


    public void addItem(String section, Item item){
        mSectionDataMap.get(mSectionMap.get(section)).add(item);

    }


    public void removeItem(String section, Item item){
        mSectionDataMap.get(mSectionMap.get(section)).remove(item);
    }


    public void removeSection(String section){
        mSectionDataMap.remove(mSectionMap.get(section));
    }



    public void notifyDataSetChanged(){
        generateDataList();
        expandListAdapter.notifyDataSetChanged();
    }



    public void generateDataList(){
        mDataArrayList.clear();
        for(Map.Entry<Section,ArrayList<Item>> entry:mSectionDataMap.entrySet()){
            Section section;
            mDataArrayList.add(section=entry.getKey());
            if (section.isExpanded){
                mDataArrayList.addAll(entry.getValue());
            }
        }
    }


    @Override
    public void onSectionStateChanged(Section section, boolean isOpen) {
        section.isExpanded=isOpen;
        notifyDataSetChanged();
    }





}
