package activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;

import com.example.huyahui.rxdemo.MainActivity;
import com.example.huyahui.rxdemo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.ScrollViewListener;
import util.MyToast;
import view.ObservableScrollView;

public class SplashActivity extends AppCompatActivity {


    private final int SPLASH_DISPLAY_LENGHT = 2500;
    @BindView(R.id.hs)
    ObservableScrollView hs;
    @BindView(R.id.img)
    ImageView img;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

//        initSplash();

        initView();

    }

    private void initView() {
        if(hs.getWidth()==0){
            MyToast.showMessage(this,"最左边");
        }

       /* hs.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if(left==0){
                    MyToast.showMessage(SplashActivity.this,"---最左边");
                }
                if(right==0){
                    MyToast.showMessage(SplashActivity.this,"---最右边");
                }
            }
        });*/


       hs.setScrollViewListener(new ScrollViewListener() {
           @Override
           public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {
               int scrollX = scrollView.getScrollX();
               int width = scrollView.getWidth();
               int scrollViewMeasuredWidth = img.getMeasuredWidth();
               if ((scrollX + width) == scrollViewMeasuredWidth) {
                   MyToast.showMessage(SplashActivity.this, "最右边---");
               }
               if (x == 0) {
                   MyToast.showMessage(SplashActivity.this, "---最左边");
               }

               if (x != 0 && (scrollX + width) != scrollViewMeasuredWidth) {
                   MyToast.showMessage(SplashActivity.this, "---中间---");
               }
           }

       });

    }


    /**
     * 延时跳转
     */
    private void initSplash() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent();
                intent.setClass(SplashActivity.this, MainActivity.class);
                SplashActivity.this.startActivity(intent);
                SplashActivity.this.finish();
                overridePendingTransition(R.anim.put_up_in, R.anim.put_up_out);
            }
        }, SPLASH_DISPLAY_LENGHT);
    }


}
