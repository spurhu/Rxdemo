package activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.huyahui.rxdemo.KeyboardActivity;
import com.example.huyahui.rxdemo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import view.NumberKeyboardView;

import static com.example.huyahui.rxdemo.KeyboardActivity.EDIT_MOBILE;

/**
 * Demo  class
 *
 * @author: huyahui
 * @date: 2018/1/20
 */
public class EditActivity extends AppCompatActivity implements NumberKeyboardView.OnNumberClickListener {


    @BindView(R.id.tv_number)
    TextView tvNumber;
    @BindView(R.id.number_board)
    NumberKeyboardView numberBoard;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.edit_pwd)
    EditText editPwd;
    private String str = "";
    private String editStr = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        ButterKnife.bind(this);
        numberBoard.setOnNumberClickListener(this);


        str=getIntent().getStringExtra(EDIT_MOBILE);
        if(!TextUtils.isEmpty(str)){
            editPwd.setText(str);
        }
        editPwd.addTextChangedListener(textWatcher);
    }




    @OnClick(R.id.btn_login)
    public void onViewClicked() {
        editStr=editPwd.getText().toString().trim();
        Intent intent = new Intent(EditActivity.this, KeyboardActivity.class);
        intent.putExtra(EDIT_MOBILE, editStr);
        startActivity(intent);
    }


    private TextWatcher textWatcher=new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            editStr=s.toString();
            Log.d("edit","----editStr---"+editStr);
            Log.d("edit","----editPwd---"+editPwd.getText().toString().trim());
        }
    };


    /**
     * 自定义键盘
     * @param number
     */
    @Override
    public void onNumberReturn(String number) {
        str += number;
        setTextContent(str);
    }

    /**
     * 自定义键盘
     */
    @Override
    public void onNumberDelete() {
        if (str.length() <= 1) {
            str = "";
        } else {
            str = str.substring(0, str.length() - 1);
        }
        setTextContent(str);

    }

    private void setTextContent(String content) {
        tvNumber.setText(content);
    }


}
