package view;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

/**
 * ******************************
 *
 * @author nihao
 * @descriptions Rxdemo
 * @email hujourney365@gmail.com
 * @date 2018/9/12
 * ******************************
 */
public class MyVideoView extends VideoView {

    public MyVideoView(Context context) {
        super(context);
    }

    public MyVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthSize = getDefaultSize(0, widthMeasureSpec);
        int heightSize = getDefaultSize(0, heightMeasureSpec);
        setMeasuredDimension(widthSize,heightSize);

    }
}
