package view;

import android.animation.TypeEvaluator;

/**
 * @author: huyahui
 * @date: 2018/3/1
 */

public class PointEvaluator implements TypeEvaluator {
    //第一个参数fraction非常重要，这个参数用
    //第二第三个参数分别表示动画的初始值和结束值。那么上述代码的逻辑就比较清晰了
    //用结束值减去初始值，算出它们之间的差值，然后乘以fraction这个系数，再加上初始值，那么就得到当前动画的值了。

    /**
     *
     * @param fraction 表示动画的完成度的，我们应该根据它来计算当前动画的值应该是多少
     * @param startValue 动画的初始值
     * @param endValue   动画的结束值
     * @return  当前动画的值=（结束值-开始值）* 完成度
     */
    @Override
    public Object evaluate(float fraction, Object startValue, Object endValue) {

        Point startPoint=(Point)startValue;
        Point endPoiint=(Point)endValue;
        //先是将startValue和endValue强转成Point对象，然后同样根据fraction来计算当前动画的x和y的值，
        //最后组装到一个新的Point对象当中并返回。
        float x=startPoint.getX()+fraction*(endPoiint.getX()-startPoint.getX());
        float y=startPoint.getY()+fraction*(endPoiint.getY()-startPoint.getY());
        Point point=new Point(x,y);
        return point;
    }



}
