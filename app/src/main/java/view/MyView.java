package view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.RegionIterator;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * @author: huyahui
 * @date: 2018/3/12
 */
public class MyView extends View {

    Context mContext;

    public MyView(Context context) {
        super(context);
        mContext=context;
    }

    public MyView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MyView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //1.设置画笔的属性
        Paint paint=new Paint();
        //坑锯齿功能
        paint.setAntiAlias(true);
        paint.setColor(Color.BLUE);
        paint.setStyle(Paint.Style.FILL);
        //设置文字方式
        paint.setTextAlign(Paint.Align.RIGHT);
        paint.setTextSize(32);
        //画笔的宽度
        paint.setStrokeWidth(5);
        //添加阴影
        paint.setShadowLayer(10,15,15,Color.GREEN);

        //2.画布的属性
        canvas.drawRGB(255, 255, 255);
        //画圆
//        canvas.drawCircle(190,200,150,paint);
//        canvas.drawLine(190,200,150,200,paint);
        //多条线
//        float []pts={50,50,400,50,50,400,400,400};
//        canvas.drawLines(pts,paint);

        //多个点
        float[] f = {10, 10, 100, 100, 200, 200, 400, 400};
//        canvas.drawPoints(f,2,4,paint);

        //矩形
//        Rect rect=new Rect(50,50,400,400);
//        canvas.drawRect(rect,paint);

        //圆角矩形
        /*RectF rectF=new RectF(100,60,300,100);
        canvas.drawRoundRect(rectF,50,100,paint);*/

        //圆形
//        canvas.drawCircle(100,100,50,paint);

       /* RectF rectF=new RectF(100,20,400,200);
        canvas.drawArc(rectF,0,90,true,paint);*/

        /*Path path = new Path();

        path.moveTo(10, 10); //设定起始点
        path.lineTo(10, 100);//第一条直线的终点，也是第二条直线的起点
        path.lineTo(300, 100);//画第二条直线
        path.lineTo(500, 100);//第三条直线
        path.close();//闭环*/

//      canvas.drawPath(path, paint);

        //先创建两个大小一样的路径
        //第一个逆向生成 CCW
        Path CCWRectpath = new Path();
        RectF rect1 =  new RectF(50, 50, 240, 200);
        CCWRectpath.addRect(rect1, Path.Direction.CCW);

        //第二个顺向生成  CW
        Path CWRectpath = new Path();
        RectF rect2 =  new RectF(290, 50, 480, 200);
        CWRectpath.addRect(rect2, Path.Direction.CW);

        //先画出这两个路径
//        canvas.drawPath(CCWRectpath, paint);
//        canvas.drawPath(CWRectpath, paint);

        //依据路径写出文字
//        String text="风萧萧兮易水寒，壮士一去兮不复返";
//        paint.setColor(Color.GRAY);
//        paint.setTextSize(35);
//        canvas.drawTextOnPath(text, CCWRectpath, 0, 18, paint);//逆时针生成
//        canvas.drawTextOnPath(text, CWRectpath, 0, 18, paint);//顺时针生成


        /*Path path1=new Path();
        RectF rectF=new RectF(50,50,240,200);
        //圆角的椭圆的横轴半径；所产生圆角的椭圆的纵轴半径
        path1.addRoundRect(rectF,40,15, Path.Direction.CCW);
        RectF rectF1=new RectF(290,50,480,200);
        //x1,y1对应第一个角的（左上角）
        float radio[]={10,15,20,25,30,35,40,45};
        path1.addRoundRect(rectF1,radio, Path.Direction.CCW);
        canvas.drawPath(path1,paint);*/

        //圆形路径
        /*Path path1=new Path();
        path1.addCircle(200,200,100, Path.Direction.CCW);
        canvas.drawPath(path1,paint);
        //依据路径写出文字
        String text2="风萧萧兮易水寒，壮士一去兮不复返";
        paint.setColor(Color.GRAY);
        paint.setTextSize(35);
        //逆时针生成
        canvas.drawTextOnPath(text, path1, 0, 18, paint);*/

       /* Path path1=new Path();
        RectF rectF=new RectF(50,50,240,200);
        path1.addOval(rectF, Path.Direction.CCW);
        canvas.drawPath(path1,paint);*/



       /* Path path1=new Path();
       RectF rectF=new RectF(60,60,240,200);
       //弧线
       path1.addArc(rectF,0,100);
       canvas.drawPath(path1,paint);*/

        Path path = new Path();
        RectF rectF = new RectF(60, 60, 300, 400);
//      path.addOval(rectF, Path.Direction.CCW);
//      path.addRect(rectF, Path.Direction.CCW);
//      canvas.drawPath(path,paint);
        //圆角矩形
//      canvas.drawRoundRect(rectF,30,50,paint);
        //二次贝塞尔曲线
//        path.quadTo(80,80,400,300);
        //三次
        path.cubicTo(50, 50, 400, 250, 400, 100);

//        canvas.drawPath(path, paint);


        //文字样式的设置
        //是否粗体
//        paint.setFakeBoldText(true);
        //下划线
//        paint.setUnderlineText(true);
        //文字倾斜
//        paint.setTextSkewX((float)-0.25);
        //设置待删除效果
//        paint.setStrikeThruText(true);
        //水平拉伸
//        paint.setTextScaleX(2);


//        canvas.drawText("冬天里的一把火",2,4,10,500,paint);
        float[] floats=new float[]{80,100,80,200,80,300,80,400};
//        canvas.drawPosText("万家乐网络",floats,paint);

        Region region=new Region(10,10,100,100);
        drawRegion(canvas,region,paint);


    }



    private void drawRegion(Canvas canvas,Region region,Paint paint){
        RegionIterator iterator=new RegionIterator(region);
        Rect rect=new Rect();
        while(iterator.next(rect)){
            canvas.drawRect(rect,paint);
        }
    }





}
