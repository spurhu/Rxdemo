package view;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

/**
 * @author: huyahui
 * @date: 2018/2/26
 */

public class ObservableWebView extends WebView {

    private OnScrollChangeCallback onScrollChangeCallback;

    public ObservableWebView(Context context) {
        super(context);
    }

    public ObservableWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ObservableWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if(onScrollChangeCallback!=null){
            onScrollChangeCallback.onScroll(l,t,oldl,oldt);
        }
    }

    public OnScrollChangeCallback getOnScrollChangeCallback() {
        return onScrollChangeCallback;
    }

    public void setOnScrollChangeCallback(OnScrollChangeCallback onScrollChangeCallback) {
        this.onScrollChangeCallback = onScrollChangeCallback;
    }

    public static interface OnScrollChangeCallback{
        public void onScroll(int l,int t,int oldl,int oldt);
    }

}
