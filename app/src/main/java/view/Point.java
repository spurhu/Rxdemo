package view;

/**
 * @author: huyahui
 * @date: 2018/3/1
 */

public class Point {
    //两个参数记录坐标
    private float x;
    private float y;

    public Point(float x, float y) {
        this.x = x;
        this.y = y;
    }
    //获取坐标
    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }
}







