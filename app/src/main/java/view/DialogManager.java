package view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.example.huyahui.rxdemo.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Title:DialogManager
 * Package:com.tomcat360.view.mydialog
 * Description:TODO
 * Author: wwh@tomcat360.com
 * Date: 16/5/25
 * Version: V1.0.0
 * 版本号修改日期修改人修改内容
 */
public class DialogManager {
    public static AlertDialog update_msgdlg;
    private static List<View> viewList;
    private  static MyViewPagerAdapter adapter;


    public static void showDialog(Context context, String title, String msg, String btnStr, DialogInterface.OnClickListener listener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setNegativeButton("取消", null);
        builder.setPositiveButton(btnStr, listener);
        builder.show();
    }

    /**
     * 不强制更新
     * @param context
     * @param msg
     * @param clickListener
     */
    public static void showConfirmDialog(Context context, String msg, View.OnClickListener clickListener) {
        View view1,view2,view3;
        viewList=new ArrayList<>();
//        LayoutInflater inflater=LayoutInflater.from(context);
        LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);;
        view1 = inflater.inflate(R.layout.dialog_home01, null);
        view2 = inflater.inflate(R.layout.dialog_home02, null);
        view3 = inflater.inflate(R.layout.dialog_home03, null);

        viewList.add(view1);
        viewList.add(view2);
        viewList.add(view3);

        adapter=new MyViewPagerAdapter(viewList);

        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyDialog);
        View view = LayoutInflater.from(context).inflate(R.layout.update_dialog, null);
        builder.setView(view);
        ViewPager vp=(ViewPager) view.findViewById(R.id.viewpager);
        ImageView cancel = (ImageView) view.findViewById(R.id.img_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update_msgdlg.dismiss();
            }
        });
        builder.setCancelable(false);
        update_msgdlg = null;
        update_msgdlg = builder.create();
        update_msgdlg.setCancelable(true);
        update_msgdlg.setCanceledOnTouchOutside(true);
        update_msgdlg.show();

        //dialog 的宽度和屏幕等宽
        Window window=update_msgdlg.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.getDecorView().setPadding(0,0,0,0);
        WindowManager.LayoutParams layoutParams=window.getAttributes();
        layoutParams.width=WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height=WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(layoutParams);

        vp.setAdapter(adapter);

    }


    public static class MyViewPagerAdapter extends PagerAdapter{
        private List<View> mListViews;

        public MyViewPagerAdapter(List<View> mListViews) {
            this.mListViews = mListViews;//构造方法，参数是我们的页卡，这样比较方便。
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object)   {
            container.removeView(mListViews.get(position));//删除页卡
        }


        @Override
        public Object instantiateItem(ViewGroup container, int position) {  //这个方法用来实例化页卡
            container.addView(mListViews.get(position), 0);//添加页卡
            return mListViews.get(position);
        }

        @Override
        public int getCount() {
            return  mListViews.size();//返回页卡的数量
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0==arg1;//官方提示这样写
        }
    }






    /**
     * 强制更新
     * @param context
     * @param msg
     * @param clickListener
     */
    public static void showConfirmDialogUnCancelable(Context context, String msg, View.OnClickListener clickListener) {

      /*  final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyDialog);
        View view = LayoutInflater.from(context).inflate(R.layout.update_dialog, null);
        builder.setView(view);
        TextView tvMsg = (TextView) view.findViewById(R.id.tv_msg);
        TextView update = (TextView) view.findViewById(R.id.tv_update);
        TextView tv_line = (TextView) view.findViewById(R.id.tv_line);
        ImageView cancel = (ImageView) view.findViewById(R.id.img_cancel);
        tv_line.setVisibility(View.GONE);
        cancel.setVisibility(View.GONE);
        tvMsg.setText(msg);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update_msgdlg.dismiss();
            }
        });
        update.setOnClickListener(clickListener);
        builder.setCancelable(false);
        update_msgdlg = null;
        update_msgdlg = builder.create();
        update_msgdlg.setCancelable(false);
        update_msgdlg.setCanceledOnTouchOutside(false);
        update_msgdlg.show();*/
    }


}
