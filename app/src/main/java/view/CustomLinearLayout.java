package view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.LinearLayout;

/**
 * @author: huyahui
 * @date: 2018/2/28
 */

public class CustomLinearLayout extends LinearLayout {

    private static final String TAG = "ViewsActivity";

    public CustomLinearLayout(Context context) {
        super(context);
    }

    public CustomLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Log.d(TAG, "dispatchTouchEvent: ---事件分发---这是CustomLinearLayout");
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        Log.d(TAG, "onInterceptTouchEvent: ---事件拦截---这是CustomLinearLayout");
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d(TAG, "onTouchEvent: ---事件消费---这是CustomLinearLayout");
        return super.onTouchEvent(event);

        // 点击Linearalyout时,
        // 如果返回的是true,事件被Linearalyout消费，不向下传递
        // 若返回的是false时，事件被Linearalyout消费，向下传递，最终会被Activity消费哦
    }







}
