package Expand;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.huyahui.rxdemo.R;

import java.util.ArrayList;

import entity.Item;
import entity.Section;

/**
 * @author: huyahui
 * @date: 2018/5/3
 */

public class ExpandListAdapter extends RecyclerView.Adapter<ExpandListAdapter.ViewHolder> {

    /**
     *  view type
     */
    private static final int VIEW_TYPE_SECTION = R.layout.layout_expand_section;
    private static final int VIEW_TYPE_ITEM = R.layout.layout_expand_item;

    private final Context mContext;
    private ArrayList<Object> mDataArrayList;

    private final ItemClickListener mItemClickListener;
    private final SectionStateChangeListener mSectionStateChangeListener;


    public ExpandListAdapter(Context mContext,
                             ArrayList<Object> mDataArrayList,
                             ItemClickListener mItemClickListener,
                             SectionStateChangeListener mSectionStateChangeListener,
                             final GridLayoutManager gridLayoutManager) {

        this.mContext = mContext;
        this.mDataArrayList = mDataArrayList;
        this.mItemClickListener = mItemClickListener;
        this.mSectionStateChangeListener = mSectionStateChangeListener;
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return isSection(position)?gridLayoutManager.getSpanCount():1;
            }
        });
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder=new ViewHolder(LayoutInflater.from(mContext).inflate(viewType, parent, false), viewType);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        switch(holder.viewType){
            case VIEW_TYPE_ITEM:

                final Item item=(Item)mDataArrayList.get(position);
                holder.tvItem.setText(item.getName());
                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClicked(item);
                    }
                });

                break;
            case VIEW_TYPE_SECTION:
                final Section section=(Section)mDataArrayList.get(position);
                holder.tvSection.setText(section.getName());
                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClicked(section);
                        toggleButtonClick(holder,section);
                    }
                });
                toggleButtonClick(holder, section);

                break;
            default:
                    break;
        }
    }

    /**
     * 回话按钮点击事件
     * @param holder
     * @param section
     */
    private void toggleButtonClick(ViewHolder holder, final Section section) {
        holder.togButton.setChecked(section.isExpanded);
        holder.togButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mSectionStateChangeListener.onSectionStateChanged(section,isChecked);
            }
        });
    }


    @Override
    public int getItemViewType(int position) {
        if (isSection(position)){
            return VIEW_TYPE_SECTION;
        }else{
            return VIEW_TYPE_ITEM;
        }
    }

    private boolean isSection(int position) {
        return mDataArrayList.get(position) instanceof Section;
    }

    @Override
    public int getItemCount() {
        return mDataArrayList==null?0:mDataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        View view;
        int viewType;
        //item
        private TextView tvItem;
        //section
        private TextView tvSection;
        private ToggleButton togButton;

        public ViewHolder( View view, int viewType) {
            super(view);
            this.view = view;
            this.viewType = viewType;
            if (viewType==VIEW_TYPE_ITEM){
                tvItem=(TextView)view.findViewById(R.id.text_item);
            }else{
                tvSection=(TextView)view.findViewById(R.id.text_section);
                togButton=(ToggleButton)view.findViewById(R.id.toggle_button_section);
            }

        }
    }












}
